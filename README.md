#Conseil-citoyen

Les scripts Conseil-Citoyen sont destiné à alimenter le ConseilCitoyen.be

La fonction de base est de prendre la liste des titres des points à l'ordre du jour d'une assemblée délibérante 
pour créer des pages web à propos de l'ordre du jour de la séance avec des pages liées pour chaque objet et des liens vers les dossiers liées.
Le script est nommé odj_lst_2_dw.py

Le fonctionnement du script est un peu inhabituel. Il vous remplace pour interragir avec Firefox. Vous verrez une fenêtre Firefox s'ouvrir 
et les actions souris et clavier se dérouler à votre place.

##Pré-requis
  - Python > 3.6
    * lxml
    * selenium

Un compte contributeur dans ConseilCitoyen.be est obtenu par compagnonnage auprès d'info@conseilCitoyen.be

##Installation

Il suffit de faire un clone du dépot gitlab.

#Utilisation

A chaque nouvelle réunion il faut déposer trois fichiers dans le dossier ./Data. 

Pour aider au traitement une série de script de pré-traitement sont utilisés

Dans le dossier il faut les fichiers : 
  * .lst avec la liste des points à l'ordre du jour.
  * .yaml avec les métas informations (date de publication, courriel de contact, ....). On s'inspire du fichier .yaml dans ./src/Template/Achives/...
  * -src.??? le document d'origine
  
  