'''
Created on 25 févr. 2018

@author: patrick
'''
import os
import sys

import yaml
import yaml_orderedDict

import collections

lib_path = os.path.abspath(os.path.join('..', 'X_lib', 'src'))
# print(lib_path)
sys.path.append(lib_path)
from x_dw_tag import *


class Pirate(object):
    '''
    classdocs
    '''

    def guessed_poi(self, str_titre, odct_key_poi=None, odct_cfg=None):

        # filter
        _odct_poi_pages = None
        # if str_titre.lower().find('asbl') != -1:
        #     print(str_titre)
        for _str_key, v in odct_key_poi.items():
            _is_in_alias = False
            for _str_alias in v['Alias']:
                # print(_str_alias)
                if _str_alias is not None and _str_alias != '' and str_titre.find(_str_alias) != -1:
                    _is_in_alias = True
                    break
            if str_titre.find(_str_key) != -1 or _is_in_alias is True:
                if _odct_poi_pages is None:
                    _odct_poi_pages = collections.OrderedDict()
                _odct_poi_pages[_str_key] = collections.OrderedDict()
                _odct_poi_pages[_str_key]['link_base'] = odct_key_poi[_str_key]['link_base']
                _odct_poi_pages[_str_key]['slug'] = odct_key_poi[_str_key]['slug']
        # sort
        if _odct_poi_pages is not None:
            #  _odct_poi = [(k, v) for k, v in _odct_poi.items()] # python > 3.6
            _odct_poi_pages = collections.OrderedDict(sorted(_odct_poi_pages.items(), key=lambda t: t[1]))

        # link section
        _str_links_out = ''
        _str_head = H(2, "Points d'intérêt")
        if _odct_poi_pages is None:
            _str_head = CM(_str_head).replace('\n', '') + '\n' + '\n'
        _str_links_out += _str_head
        _str_links_out += CM('begin poi')
        _lst_link = []
        if _odct_poi_pages is not None:
            for _k, _v in _odct_poi_pages.items():
                _lst_link.append('[[%(path)s|%(poi)s]]' % {'path': os.path.join(odct_key_poi[_k]['link_base'],
                                                                                odct_key_poi[_k]['slug'],
                                                                                'start'),
                                                           'poi': _k})
            _str_links_out += ', '.join(_lst_link)
        _str_links_out += CM('end poi')
        # print(_str_links_out)
        # _odct_poi_flt['link'] = _str_links_out

        # create pages and add to odct_poi_flt
        # Class=tag versus instance ?
        if _odct_poi_pages is not None:
            for k, v in _odct_poi_pages.items():
                _str_new_page = ''
                # Change if org or role or ???
                _str_new_page += H(1, k) + '\n'
                _str_new_page += CM('begin backlink')
                _str_backlink = ' * '
                _str_new_page += '\n'
                _str_new_page += CM('end backlink')
                _odct_poi_pages[k]['dw_page'] = _str_new_page

        # update page (backlink)
        if _odct_poi_pages is not None:
            for k, v in _odct_poi_pages.items():
                _str_backlink = ''
                _str_backlink_label = odct_cfg['Cfg']['N-gram'][odct_cfg['Cfg']['Path']['org']]['name']
                _str_backlink_label += ' - '
                _str_backlink_label = odct_cfg['Cfg']['N-gram'][odct_cfg['Cfg']['Path']['org']]['class_and_name']
                _str_backlink_label += '\n'
                _str_backlink_label = str_titre + '\n'

                _str_pathfilename_dw_link = os.path.join('http://',
                                                         odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                         odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                         odct_cfg['Cfg']['Path']['org_type'].lower()
                                                         + odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                         'meet',
                                                         odct_cfg['Cfg']['Path']['docname_src'].lower(),
                                                         'start',
                                                         )
                _str_dw_link = '[[%(path)s|%(label)s]]' % {'path': _str_pathfilename_dw_link,
                                                           'label': _str_backlink_label,
                                                           }
                _odct_poi_pages[k]['back_link'] = _str_dw_link

        return _str_links_out, _odct_poi_pages

    def guessed_tag(self, str_titre, odct_line_tag=None):
        if odct_line_tag is None:
            odct_line_tag = {'police': 'incivilité',
                             'finance': 'redevances',
                             'finance': "Fabrique d'Eglise",
                             'logement': 'urbanisme',
                             'Oip': 'délégué',
                             }
        _lst_tag = None
        for k, str_key in odct_line_tag.items():
                if str_titre.find(str_key) != -1:
                    if _lst_tag is None:
                        _lst_tag = []
                    _lst_tag.append(k)
        if _lst_tag is not None:
            _lst_tag = list(set(_lst_tag))
            _lst_tag.sort()
        _str_out = ''
        _str_out += H(2, 'Tags') + '\n'
        _str_out += CM('begin of tag list')
        # _str_out += 'Tags: ' + ', '.join(_lst_tag)
        _str_out_line = '{{tag>}}'
        if _lst_tag is not None:
            _lst_tag = list(set(_lst_tag))
            _lst_tag.sort()
            _str_out_line = '{{tag>' + ', '.join(_lst_tag) + '}}'
        _str_out += _str_out_line
        _str_out += '\n'
        _str_out += CM('end of tag list')
        return _str_out

    def guessed_geoloc_filter(self, str_titre, odct_key_geo):
        # filter
        _odct_geo_pages = None
        # if str_titre.lower().find('rue') != -1:
        #     print(str_titre)
        for _str_key, v in odct_key_geo.items():
            _is_in_alias = False
            for _str_alias in v['Alias']:
                # print(_str_alias)
                if _str_alias is not None and _str_alias != '' and str_titre.find(_str_alias) != -1:
                    _is_in_alias = True
                    break
            if str_titre.find(_str_key) != -1 or _is_in_alias is True:
                if _odct_geo_pages is None:
                    _odct_geo_pages = collections.OrderedDict()
                _odct_geo_pages[_str_key] = collections.OrderedDict()
                _odct_geo_pages[_str_key]['lat_lon'] = odct_key_geo[_str_key]['lat_lon']
                _odct_geo_pages[_str_key]['link_base'] = odct_key_geo[_str_key]['link_base']
                _odct_geo_pages[_str_key]['slug'] = odct_key_geo[_str_key]['slug']
        # sort
        if _odct_geo_pages is not None:
            #  _odct_geo_pages = [(k, v) for k, v in _odct_geo_pages.items()] # python > 3.6
            # _odct_geo_pages = collections.OrderedDict(sorted(_odct_geo_pages.items(), key=lambda t: t[1]))
            _odct_geo_pages = collections.OrderedDict(sorted(_odct_geo_pages.items()))
        return _odct_geo_pages

    def guessed_geoloc_links(self, odct_key_geo, _odct_geo_pages):
        _str_links_out = ''
        _str_head = H(2, "Geolocalisation")
        if _odct_geo_pages is None:
            _str_head = CM(_str_head).replace('\n', '') + '\n' + '\n'
        _str_links_out += _str_head
        _str_links_out += CM('begin geo')
        _lst_link = []
        if _odct_geo_pages is not None:
            for _k, _v in _odct_geo_pages.items():
                _lst_link.append('[[%(path)s|%(geo)s]]' % {'path': os.path.join(odct_key_geo[_k]['link_base'],
                                                                                odct_key_geo[_k]['slug'],
                                                                                'start'),
                                                           'geo': _k})
            _str_links_out += ', '.join(_lst_link)
        _str_links_out += CM('end geo')
        return _str_links_out

    def guessed_geoloc_page(self, odct_geo_pages):
        # create pages and add to odct_poi_flt
        # Class=tag versus instance ?
        if odct_geo_pages is not None:
            for str_key, v in odct_geo_pages.items():
                _str_new_page = ''
                # Change if org or role or ???
                _str_new_page += H(1, str_key) + '\n'
                _str_new_page += H(2, 'Cartes') + '\n'
                _str_new_page += CM('begin geoservices')
                _str_domain = 'www.openstreetmap.org'
                _str_url = 'https://%(domain)s/?mlat=%(lat)s&mlon=%(lon)s&zoom=19' % {'domain': _str_domain,
                                                                                      'lat': v['lat_lon'].split(', ')[0],
                                                                                      'lon': v['lat_lon'].split(', ')[1],
                                                                                      }
                _str_anchor = '[[%(url)s|%(label)s]]\n' % {'label': str_key,
                                                           'url': _str_url,
                                                           }
                _str_new_page += _str_anchor
                _str_new_page += CM('end geoservices')
                _str_new_page += H(2, 'Documents liés') + '\n'
                _str_new_page += CM('begin backlink')
                _str_new_page += CM('end backlink')
                odct_geo_pages[str_key]['dw_page'] = _str_new_page
        return odct_geo_pages

    def guessed_geoloc_backlink(self, _odct_poi_pages, odct_cfg):
        if _odct_poi_pages is not None:
            for str_key, v in _odct_poi_pages.items():
                _str_backlink = ''
                _str_backlink_label = odct_cfg['Cfg']['Ngram'][odct_cfg['Cfg']['Path']['org_parent']]
                _str_backlink_label += ' - '
                _str_backlink_label = odct_cfg['Cfg']['Ngram'][odct_cfg['Cfg']['Path']['org_full'][:10]]
                _str_backlink_label += '\n'
                _str_backlink_label = str_key + '\n'

                _str_pathfilename_dw_link = os.path.join('http://',
                                                         odct_cfg['Cfg']['Http']['domain'],
                                                         odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                         odct_cfg['Cfg']['Path']['org_type'].lower()
                                                         + odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                         'meet',
                                                         odct_cfg['Cfg']['Path']['docname_src'].lower(),
                                                         'start',
                                                         )
                _str_dw_link = '[[%(path)s|%(label)s]]' % {'path': _str_pathfilename_dw_link,
                                                           'label': _str_backlink_label,
                                                           }
                _odct_poi_pages[str_key]['back_link'] = _str_dw_link
        return _odct_poi_pages

    def guessed_geoloc(self, str_titre, odct_key_geo, odct_cfg):
        '''
        '''
        _odct_geo_pages = self.guessed_geoloc_filter(str_titre, odct_key_geo)
        _str_links_out = self.guessed_geoloc_links(odct_key_geo, _odct_geo_pages)
        _odct_geo_pages = self.guessed_geoloc_page(_odct_geo_pages)
        _odct_geo_pages = self.guessed_geoloc_backlink(_odct_geo_pages, odct_cfg)
        return _str_links_out, _odct_geo_pages

    def guess_is_information(self, odct_data_topic):
        _lst_is_information_key = ['Information', 'Communication', 'interpellation', 'interpeller']
        _is_information = False
        for _str_key in _lst_is_information_key:
            if isinstance(odct_data_topic, str):
                if odct_data_topic.find(_str_key) != -1:
                    _is_information = True
            elif isinstance(odct_data_topic, dict):
                if odct_data_topic['titre'].find(_str_key) != -1:
                    _is_information = True
            else:
                if odct_data_topic['titre'].find(_str_key) != -1:
                    _is_information = True
        return _is_information

    def tags_to_args(self, lst_tags):

        _dct_args = {'pour': [],
                     'contre': [],
                     'abstention': [],
                     }
        return _dct_args

    def add_alternative_evaluations(self, lst_tags=None):
        _str_out = ''
        _str_out += H(3, 'Evaluation') + '\n'
        _str_out += 'Est-ce que les arguments sont vrais, pertinents et importants ?\n'
        _str_out += CM('begin of evaluations')
        _str_out += "^ Aternative ^ Critère ^ Evaluation de X"
        _str_out += "|\n"

        _str_out += "| <color /yellow>Pour</color>"
        _str_out += "| <color /yellow>j'appuie le collège</color> "
        _str_out += "| Vrai - <color lightgreen> peu pertinent</color> - <color lightpink> peu important </color>"
        _str_out += "|\n"

        _str_out += "| "
        _str_out += "| <color /yellow>le dossier est complet et convaincant</color> "
        _str_out += "| Vrai - <color darkgreen> tres pertinent</color> - <color darkred> très important </color>"
        _str_out += "|\n"

        _str_out += "| <color /lightcyan>Contre</color>"
        _str_out += "| <color /lightcyan>je suis contre le collège</color> "
        _str_out += "| Vrai - <color lightgreen> peu pertinent </color> - <color lightpink>  peu important  </color>"
        _str_out += "|\n"

        _str_out += "| "
        _str_out += "| <color /lightcyan>l'information est absente ou insuffisante </color> "
        _str_out += "| Vrai - <color green> pertinent </color> - <color red>  important  </color>"
        _str_out += "|\n"

        _str_out += "| "
        _str_out += "| <color /lightcyan>la transparence administrative n'a pas été respectée </color> "
        _str_out += "| Vrai - <color green> pertinent </color> - <color red>  important  </color>"
        _str_out += "|\n"

        _str_out += "| <color /mistyrose>Abstention</color>"
        _str_out += "| <color /mistyrose>la justification doit être reprise au procès-perbal </color> "
        _str_out += "| Vrai - <color green> pertinent </color> - <color red>  important  </color> "
        _str_out += "|\n"

        if lst_tags is not None and 'Approbation du procès verbal' in lst_tags:
            _str_out += "| "
            _str_out += "| <color /mistyrose>j'étais absent lors de cette réunion</color> "
            _str_out += "| Vrai - <color green> pertinent </color> - <color darkred>  très important  </color> "
            _str_out += "|\n"

        _str_out += "| "
        _str_out += "| <color /mistyrose>le quorum n'est pas atteint</color> "
        _str_out += "| Faux (pas de vote sans quorum) "
        _str_out += "|\n"

        _str_out += "| "
        _str_out += "| "
        _str_out += "^ Conclusion ??? "
        _str_out += "|\n"
        _str_out += CM('end of evaluations')
        return _str_out

    def add_alternative_arguments(self, lst_tags=None):
        # https://www.dokuwiki.org/plugin:color
        _str_out = ''
        _str_out = '\n'
        _str_out += H(3, 'Argumentation') + '\n'
        _str_out += '[[https://wikicitoyen.be/faq/start#ou_discuter_a_propos_du_wikicitoyen|Aide]]\n'
        _str_out += CM('begin of arguments')
        _str_out += '  * ' + B('<color /yellow> Pour </color>') + '\n'
        _str_out += "    * Parce que <color /yellow>j'appuie le collège</color>\n"
        _str_out += "    * Parce que <color /yellow>le dossier est complet et convaincant</color>\n"
        _str_out += '  * ' + B('<color /lightcyan> Contre </color>') + '\n'
        _str_out += "    * Parce que <color /lightcyan>je suis contre le collège</color>\n"
        _str_out += "    * Parce que <color /lightcyan>l'information est absente ou insuffisante</color>\n"
        _str_out += "    * Parce que <color /lightcyan>la transparence administrative n'a pas été respectée</color>\n"
        _str_out += '  * ' + B('<color /mistyrose> Abstention </color>') + '\n'
        _str_out += "    * Parce que <color /mistyrose>justification reprise au procès-verbal</color>\n"
        if lst_tags is not None and 'Approbation du procès verbal' in lst_tags:
            _str_out += "    * Parce que <color /mistyrose>j'étais absent lors de cette réunion</color>\n"
        _str_out += "    * Parce que <color /mistyrose>le quorum n'est pas atteint</color>\n"
        _str_out += CM('end of arguments')
        '''
        _str_out += H(4, 'Contibuteurs') + '\n'
        _str_out += 'Avatars: robot_pirate, \n'
        '''
        _str_out += self.add_alternative_evaluations()
        _str_out += '\n'
        return _str_out

    def add_argmap_data(self, odct_cfg):  # , str_filename_yaml, lst_pathfilename_write, lst_pathfilename_read):
        _str_semantic = '\n'
        _str_semantic += "<hidden Carte d'arguments>" + '\n'
        _str_semantic += '<code yaml>' + '\n'
        _str_semantic += 'ArgMap:' + '\n'
        _str_semantic += '  @context:' + '\n'
        _str_semantic += '    schema: http://schema.org/' + '\n'
        _str_semantic += '  Alt:' + '\n'
        _str_semantic += '  - value: dummy' + '\n'
        _str_semantic += '    Args: ' + '\n'
        _str_semantic += '    - value: dummy too' + '\n'
        _str_semantic += '\n'
        _str_semantic += '</code>' + '\n'
        _str_semantic += '</hidden>' + '\n'
        return _str_semantic

    def info_complementaires(self):
        _str_out = ''
        _str_out = '\n'
        _str_out += H(3, 'Informations complémentaires') + '\n'
        _str_out += "Certaines informations n'appuient pas un choix mais méritent d'être publiées\n"
        return _str_out

    def add_abonnement(self):
        pass

    def add_localisation(self):
        pass

    def close_vote(self):
        pass

    def add_pool(self, str_dt_iso, str_org, str_grp, str_topic):
        _str_out = '\n'
        _str_out += '<hidden Votes citoyens>'
        _str_out += '<poll %(dt_iso)s_%(org)s_%(grp)s-%(topic)s>' % {'dt_iso': str_dt_iso,
                                                                     'org': str_org,
                                                                     'grp': str_grp,
                                                                     'topic': str_topic,
                                                                     }
        _str_out += '''Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>\n'''
        _str_out += '\n' + '</hidden>' + '\n'
        _str_out += '\n'
        return _str_out

    def add_semantic(self, odct_cfg):  # , str_filename_yaml, lst_pathfilename_write, lst_pathfilename_read):
        _str_semantic = '\n'
        _str_semantic += '<hidden Sémantique>' + '\n'
        _str_semantic += '<code yaml>' + '\n'
        _str_semantic += 'Semantic:' + '\n'
        _str_semantic += '  @context:' + '\n'
        _str_semantic += '    schema: http://schema.org/ ' + '\n'
        _str_semantic += '</code>' + '\n'
        _str_semantic += '</hidden>' + '\n'
        return _str_semantic

    def __init__(self):
        '''
        Constructor
        '''
        pass
