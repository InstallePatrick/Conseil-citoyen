'''
Created on 2 mars 2018

@author: patrick
'''
import os
import sys
import shutil
import codecs
import time
import datetime
import collections
import difflib

import yaml
import yaml_orderedDict

import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
# from asyncio.tasks import wait

import x_tools
from x_selenium import X_selenium

from x_dw_tag import *
from x_yaml import X_yaml
# from x_ftp import X_ftp

# lib_path = os.path.abspath(os.path.join('..', '..', 'X_lib', 'src'))
# print(lib_path)
# sys.path.append(lib_path)

# from x_config import X_config
from x_dw import X_dw

from adm import Adm
from pirate import Pirate


def dw_login(sel_drv, str_usr, str_pwd):

    _elm_username = sel_drv.find_element_by_id("focus__this")
    _elm_username.send_keys(str_usr)

    _elm_password = sel_drv.find_element_by_name("p")
    _elm_password.send_keys(str_pwd)
    time.sleep(1)  # anyway

    # _elm_connect = sel_drv.find_element_by_xpath("//button[@type='submit']")
    # time.sleep(1)  # anyway

    _elm_submit = sel_drv.find_element_by_xpath('//body')
    _elm_submit.send_keys(Keys.TAB + Keys.TAB + Keys.ENTER)
    time.sleep(1)  # anyway


def dw_update_url(odct_cfg):
    print('Update_url')
    _sel_drv = webdriver.Firefox()
    # _sel_drv = webdriver.Chrome()
    _sel_drv.get_cookies()

    _lst_url = odct_cfg['Cfg']['Dw_url']

    _sel_drv.get(_lst_url[0])

    try:
        _sel_drv.find_element_by_id("focus__this")
        dw_login(_sel_drv, odct_cfg['Cfg']['Dw']['usr'], odct_cfg['Cfg']['Dw']['pwd'])
    except NoSuchElementException as e:
        pass

    for _str_url in _lst_url:
        print(_str_url)
        _sel_drv.get(_str_url)
        time.sleep(2)  # anyway


class Dw_edit(object):

    def login(self, str_url):
        '''
        Login
        '''
        self.sele.driver.maximize_window()

        # _str_config_name = self.cfg['Cch']['config_name']
        # _str_url = self.cfg['Cch'][_str_config_name]['url_topics']
        self.sele.driver.get(str_url)
        # print(self.cfg['Bos']['Selenium']['driver'].page_source)
        # X_selenium(self.cfg, 'Cch').wait_for_element_xpath('//form[@id="dw__login"]', 4)
        self.sele.wait_for_element_xpath('//a[@class="action login"]', 4)
        # time.sleep(1)
        try:
            _elm_login = self.sele.driver.find_element_by_xpath('//a[@class="action login"]')
            _elm_login.click()
            time.sleep(1)

            _cookies = self.sele.driver.get_cookies()

            _elm_username = self.sele.driver.find_element_by_xpath("//input[@name='u']")
            # _str_username = self.cfg['Cch']['Remote']['Dw']['user']
            _str_username = self.dw_login['user']

            # print(_elm_username.get_attribute('value'))
            # print(_elm_username.get_attribute('innerHTML'))
            _elm_username.send_keys(_str_username)

            _elm_password = self.sele.driver.find_element_by_xpath("//input[@name='p']")
            _str_password = self.dw_login['pswd']
            _elm_password.send_keys(_str_password)

            _elm_body = self.sele.driver.find_element_by_xpath("//body")
            _elm_body.send_keys(Keys.TAB)
            _elm_body.send_keys(Keys.TAB)
            _elm_body.send_keys(Keys.ENTER)
            time.sleep(1)
        except NoSuchElementException:  # already logged
            pass
        else:
            print('login : find_element_not_foudn')

    def selenium_windows(self):
        _dct_title_wh = {}
        for _wh in self.sele.driver.window_handles:
            self.sele.driver.switch_to.window(_wh)
            print(_wh)
            print(self.sele.driver.title)
            if self.sele.driver.title.find('File upload') != -1:
                _dct_title_wh['upload'] = self.sele.driver.current_window_handle
            elif self.sele.driver.title.find('Sélection de fichiers') != -1:
                _dct_title_wh['media'] = self.sele.driver.current_window_handle
            elif self.sele.driver.title.find('Conseil citoyen') != -1:
                _dct_title_wh['root'] = self.sele.driver.current_window_handle
            else:
                print('unknown window %s' % self.sele.driver.title)
        return _dct_title_wh

    def url_2_textarea(self, str_url_page):
        _is_connected = False
        while not _is_connected:
            try:
                self.sele.driver.get(str_url_page)
                _is_connected = True
            # except selenium.common.exceptions.WebDriverException as e:
            except WebDriverException as e:
                _xpath_retry = "//button[@id='errorTryAgain']"
                _selm_retry = self.sele.driver.find_element_by_xpath(_xpath_retry)
                # _selm_retry.send_keys(Keys.ENTER)
                _selm_retry.click()
        _str_xpath_top = '//a[@class="action top"]'
        self.sele.wait_for_element_xpath(_str_xpath_top, 8)
        time.sleep(2)
        try:
            _selm_edit = self.sele.driver.find_element_by_xpath("//a[@class='action edit']")
            _selm_edit.send_keys(Keys.ENTER)
        except NoSuchElementException as e:
            self.login(str_url_page)
            _str_xpath_top = '//a[@class="action top"]'
            self.sele.wait_for_element_xpath(_str_xpath_top, 4)
            time.sleep(2)
            try:
                _selm_edit = self.sele.driver.find_element_by_xpath("//a[@class='action edit']")
                _selm_edit.send_keys(Keys.ENTER)
            except NoSuchElementException as e:
                try:
                    _selm_edit = self.sele.driver.find_element_by_xpath("//a[@class='action create']")
                    _selm_edit.send_keys(Keys.ENTER)
                except NoSuchElementException as e:
                    _selm_edit = self.sele.driver.find_element_by_xpath("//a[@class='action draft']")
                    _selm_edit.send_keys(Keys.ENTER)
                    # _selm_cancel = self.odct_cfg['Cfg']['Sele'].driver.find_element_by_xpath("//button[@name='do[draftdel]']")
                    # _selm_draft = self.sele.driver.find_element_by_xpath("//button[@name='do[draftdel]']")
                    _selm_draft = self.sele.driver.find_element_by_xpath("//button[contains(@name, 'recover')]")
                    _selm_draft.click()
                else:
                    print('url_2_textarea : xpaths not found')
        time.sleep(3)

    def save_media(self, str_local_pathfilenmame, str_url_page):  # , str_url_media):

        self.url_2_textarea(str_url_page)

        _xpath_button_media = '//button/img[contains(@src, "lib/images/toolbar/image.png")]'
        _selm_media = self.sele.driver.find_element_by_xpath(_xpath_button_media)
        _selm_media.click()
        _xpath_selection = '//input[@name="file"]'
        self.sele.wait_for_element_xpath(_xpath_selection, 12)
        time.sleep(1)

        # popup media selector
        # self.selenium_windows()
        # _wh_media = self.sele.driver.window_handles[1]
        _dct_title_wh = self.selenium_windows()
        _wh_media = _dct_title_wh['media']
        self.sele.driver.switch_to.window(_wh_media)
        # print(self.sele.driver.page_source)
        # _xpath_upload_button = '//div[@class="qq-upload-button"]'
        # _selm_media = self.sele.driver.find_element_by_xpath(_xpath_upload_button)
        _xpath_selection = '//input[@name="file"]'
        _selm_media = self.sele.driver.find_element_by_xpath(_xpath_selection)
        _selm_media.send_keys(str_local_pathfilenmame)
        time.sleep(1)

        # TODO add a option overwrite upload files
        _xpath_upload_overwrite = '//div[@class="qq-action-container"]'  # Check box activation (overwrite)
        _selm_media = self.sele.driver.find_element_by_xpath(_xpath_upload_overwrite)
        _selm_media.click()

        _xpath_upload_button = '//button[@class="qq-upload-action"]'
        # _xpath_upload_button = '//button[@id="mediamanager__upload_button"]'
        _selm_media = self.sele.driver.find_element_by_xpath(_xpath_upload_button)
        _selm_media.send_keys(Keys.ENTER)
        # _selm_media.click()

        _str_xpath_termine = '//button[contains(text(), "Terminé")]'
        self.sele.wait_for_element_xpath(_str_xpath_termine, 10)
        time.sleep(1)
        # print(self.sele.driver.page_source)
        # _selm_termine = self.sele.driver.find_element_by_xpath(_str_xpath_termine)
        # _selm_termine.send_keys(Keys.ENTER)

        _selm_body = self.sele.driver.find_element_by_xpath('//body')
        # self.sele.wait_for_element_xpath(_xpath_selection, 10)
        # _selm_media = self.sele.driver.find_element_by_xpath(_xpath_selection)
        time.sleep(1)
        # _selm_body = self.sele.driver.find_element_by_xpath('//body')
        # _selm_body.send_keys(Keys.ALT + Keys.F4) # From Edit to View
        # _selm_body.send_keys(Keys.CONTROL + Keys.F4) # From Edit to View No modal dialog is currently open
        # _selm_body.send_keys(Keys.CONTROL + 'Q') # From Edit to View
        self.selenium_windows()
        _wh_media = _dct_title_wh['media']
        self.sele.driver.switch_to.window(_wh_media)
        self.sele.driver.close()
        self.selenium_windows()

        time.sleep(1)

        self.sele.driver.switch_to.window(self.sele.driver.window_handles[0])

        _selm_body = self.sele.driver.find_element_by_xpath('//body')
        _selm_body.send_keys(Keys.CONTROL + 'V')  # From Edit to View

    def read_page(self):
        pass

    def sentence_2_topic(self):
        pass

    def create_page(self):
        pass

    def send_content(self, selm, str_content):
        _lst_line = str_content.split('\n')
        _is_previous_without_leading_space = True
        for _str_line in _lst_line:
            if _is_previous_without_leading_space:
                selm.send_keys(_str_line + '\n')
            else:
                selm.send_keys(Keys.SHIFT + Keys.HOME)
                selm.send_keys(Keys.DELETE)
                selm.send_keys(_str_line + '\n')
            if len(_str_line) != 0 and _str_line[0] == ' ':
                _is_previous_without_leading_space = False

    def render_diff(self, old_text, new_text):
        print (old_text, old_text.__class__)
        print (new_text, new_text.__class__)
        sm = difflib.SequenceMatcher(a=old_text, b=new_text)
        out_toks = []
        for opcode, s1, e1, s2, e2 in sm.get_opcodes():
            if opcode == 'equal':
                out_toks.append(old_text[s1:e1])
            elif opcode == 'insert':
                out_toks.append('<span class="insert">' + new_text[s2:e2] + '</span>')
            elif opcode == 'delete':
                out_toks.append('<span class="delete">' + old_text[s1:e1] + '</span>')
            elif opcode == 'replace':
                out_toks.append('<span class="delete">' + old_text[s1:e1] + '</span>')
                out_toks.append('<span class="insert">' + new_text[s2:e2] + '</span>')
        return ''.join(out_toks)

    def save_content(self, str_url_page, str_content):

        # a little cleaning
        while str_content.find('\n\n\n') != -1:
            str_content = str_content.replace('\n\n\n', '\n\n')

        self.url_2_textarea(str_url_page)
        _selm_edit = self.sele.driver.find_element_by_xpath("//textarea[@id='wiki__text']")
        _str_clipboard = _selm_edit.get_attribute("value")
        if _str_clipboard[:-1] == str_content:
            # print('Equal')
            _selm_cancel = self.sele.driver.find_element_by_xpath("//button[@name='do[draftdel]']")
            _selm_cancel.send_keys(Keys.ENTER)
            time.sleep(1)
        else:
            # print('Not Equal')
            _selm_edit.send_keys(Keys.CONTROL + 'A')
            _selm_edit.send_keys(Keys.DELETE)
            self.send_content(_selm_edit, str_content)
            time.sleep(1)
            _selm_save = self.sele.driver.find_element_by_xpath("//button[@id='edbtn__save']")
            _selm_save.send_keys(Keys.ENTER)
            time.sleep(1)

    def update_content(self, str_pathfile_content, str_url):
        # self.sele.driver.get(str_url) # the state of the page must be edit mode
        _str_file = None
        with open(str_pathfile_content, 'r', encoding='utf-8') as _file_local:
            _str_file = _file_local.read()
        self.save_content(str_url, _str_file)

    def read_content(self, str_url_page):

        self.url_2_textarea(str_url_page)

        _selm_edit = self.sele.driver.find_element_by_xpath("//textarea[@id='wiki__text']")
        _str_content = _selm_edit.get_attribute("value")
        return _str_content

    def edit_content(self, str_url_page):
        _str_content = self.read_content(str_url_page)
        self.update_page(_str_content)

    def __init__(self, cfg_sele, cfg_dw_login):  # str_config_name):
        self.sele = cfg_sele
        self.dw_login = cfg_dw_login


class Mad(object):
    # Decision aid
    pass


class Woi():
    # Way of interest
    pass


class Aoi(object):
    # Area of interest
    pass


class Poi(object):
    # Point Of Interest

    def read_odct_tag_filter(self):
        odct_tag_filter = None
        # load the metadata of list
        _str_filename_tag_filter = 'tag_filter' + '.yaml'
        _str_pathfilename_doc_cfg = os.path.join(*['.',
                                                   'Config',
                                                   _str_filename_tag_filter
                                                   ]
                                                 )
        with open(_str_pathfilename_doc_cfg, 'r') as _file_in:
            odct_tag_filter = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
        return self.odct_cfg

    def create_geo_class_page(self, x_ftp, str_ftp_pathfilename_geo_page):
        # if the class is not referenced update the list
        if not x_ftp.ftp_client.path.exists(str_ftp_pathfilename_geo_page):
            _str_page = ''
            _str_page += H(1, 'Geolocalisation').replace('\n', '') + '\n'
            _str_page += H(2, "Points d'intérêt").replace('\n', '') + '\n'
            _str_page += CM('begin geolocs')
            _str_page += '\n'
            _str_page += CM('end geolocs')
            x_ftp.upload_ftp_str_page(str_ftp_pathfilename_geo_page, _str_page)

    def update_geo_class_page(self, x_ftp, _dct_tree_topic, str_path_geo_class, str_geo_slug, str_title):
        # if the class is not referenced update the list
        _str_ftp_pathfilename_geo_page = str_path_geo_class.replace(str_geo_slug, '').replace('//', '/').replace('::', ':')
        self.create_geo_class_page(x_ftp, _str_ftp_pathfilename_geo_page)
        _str_class_2_instance = '  * [[.:%(a)s:start|%(t)s]]' % {'a': str_geo_slug,
                                                                 't': str_title,
                                                                 }
        X_dw().update_dw_section_list(x_ftp, _str_ftp_pathfilename_geo_page, 'geolocs', _str_class_2_instance)

    def update_geo_backlink(self, x_ftp, _str_fullpathfilename_geo_ftp, _dct_tree_topic):
        # from poi geo to meeting topic
        _str_backlink = '  * [[%(a)s|%(t)s]] (%(o)s - %(d)s)' % {'a': _dct_tree_topic['url'],
                                                                 't': _dct_tree_topic['title'],
                                                                 'o': _dct_tree_topic['org-name'],
                                                                 'd':  _dct_tree_topic['date-time'],
                                                                 }
        X_dw().update_dw_section_list(x_ftp, _str_fullpathfilename_geo_ftp, 'backlink', _str_backlink)

        '''
        _str_page = _x_ftp.download_ftp_str_page(_str_fullpathfilename_geo_ftp)
        _lst_lines = _str_page.split('\n') # keep eol at end of string ?

        _has_link = False
        for _str_line in _lst_lines:
            if _str_line.find(_dct_tree_topic['url']) != -1:
                _has_link = True
                break
        if _has_link is False:
            _lst_str_head = []
            _is_head = True
            _lst_str_backlink = []
            _lst_str_tail = []
            _is_tail = False
            if _has_link is False:
              for _str_line in _lst_lines:
                  if  _is_head is True:
                      _lst_str_head.append(_str_line)
                      if  _str_line.find('begin backlink') != -1:
                          _is_head = False
                      continue
                  if  _str_line.find('end backlink') != -1:
                      _is_tail = True
                      _lst_str_tail.append(_str_line)
                      continue
                  if _is_tail is True:
                      _lst_str_tail.append(_str_line)
                      continue
                  _lst_str_backlink.append(_str_line)

            _lst_str_backlink.append(_str_backlink)
            # remove empty lines
            _lst_str_backlink = list(filter(bool, _lst_str_backlink))
            _lst_str_backlink.sort()
            _str_page_new = '\n'.join(_lst_str_head) + '\n'.join(_lst_str_backlink) + '\n'.join(_lst_str_tail)
            _x_ftp.upload_str(_str_page_new, _str_fullpathfilename_geo_ftp)
        '''

    def update_geo_dw_pages(self, odct_geo_pages, _dct_tree_topic):
        if odct_geo_pages is not None:
            _x_ftp = X_ftp(self.odct_cfg['Cfg']['Ftp'])
            for _k, _v in odct_geo_pages.items():
                # always relative path (starts with ..)
                _str_fullpathfilename_geo_ftp = os.path.join(self.odct_cfg['Cfg']['Ftp']['path_base'],
                                                             self.odct_cfg['Cfg']['Ftp']['path_dw_pm'] % {'pm': 'pages'},
                                                             self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                             self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                             + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                             _v['link_base'].lower()[9:],
                                                             _v['slug'].lower(),
                                                             'start.txt'
                                                             )
                if not _x_ftp.ftp_client.path.exists(_str_fullpathfilename_geo_ftp):
                    _x_ftp.upload_ftp_str_page(_str_fullpathfilename_geo_ftp, _v['dw_page'])
                # update backlink
                self.update_geo_backlink(_x_ftp, _str_fullpathfilename_geo_ftp, _dct_tree_topic)
                self.update_geo_class_page(_x_ftp, _dct_tree_topic, _str_fullpathfilename_geo_ftp, _v['slug'].lower(), _k)

    def update_poi_class(self, str_link_base, str_slug, str_titre):
        pass

    def update_poi_backlink(self, str_link_base, str_slug, str_titre):
        pass

    def sele_update_poi_dw_pages(self, odct_poi_pages):
        if odct_poi_pages is not None:
            for _k, _v in odct_poi_pages.items():
                if _v['link_base'].startswith('..'):
                    _str_fullpathfilename_https = os.path.join('http://',
                                                               self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                               self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                               self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                               + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                               _v['link_base'].lower()[9:],
                                                               _v['slug'].lower(),
                                                               'start'
                                                               )
                    _dw_edit = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
                    # str_content = _dw_edit.read_content(_str_fullpathfilename_https)
                    _dw_edit.save_content(_str_fullpathfilename_https, _v['dw_page'])
                else:
                    _str_fullpathfilename_ftp = os.path.join(self.odct_cfg['Cfg']['Ftp']['path_base'],
                                                             self.odct_cfg['Cfg']['Ftp']['path_dw_pm'] % {'pm': 'pages'},
                                                             _v['link_base'].lower(),
                                                             _v['slug'].lower(),
                                                             'start.txt'
                                                             )

    def ftp_update_poi_dw_pages(self, odct_poi_pages):
        if odct_poi_pages is not None:
            _x_ftp = X_ftp(self.odct_cfg['Cfg']['Ftp'])
            for _k, _v in odct_poi_pages.items():
                if _v['link_base'].startswith('..'):
                    _str_fullpathfilename_ftp = os.path.join(self.odct_cfg['Cfg']['Ftp']['path_base'],
                                                             self.odct_cfg['Cfg']['Ftp']['path_dw_pm'] % {'pm': 'pages'},
                                                             self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                             self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                             + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                             _v['link_base'].lower()[9:],
                                                             _v['slug'].lower(),
                                                             'start.txt'
                                                             )
                else:
                    _str_fullpathfilename_ftp = os.path.join(self.odct_cfg['Cfg']['Ftp']['path_base'],
                                                             self.odct_cfg['Cfg']['Ftp']['path_dw_pm'] % {'pm': 'pages'},
                                                             _v['link_base'].lower(),
                                                             _v['slug'].lower(),
                                                             'start.txt'
                                                             )
                if not _x_ftp.ftp_client.path.exists(_str_fullpathfilename_ftp):
                    _x_ftp.upload_ftp_str_page(_str_fullpathfilename_ftp, _v['dw_page'])
                    # update backlink
                self.update_poi_class(_v['link_base'], _v['slug'].lower(), _k)

    def __init__(self, odct_cfg):
        self.odct_cfg = odct_cfg


class Topic(object):

    def save_topic(self, str_page_dw):
        _str_path_out = os.path.join('..',
                                     'Data',
                                     'Archives',
                                     'g'
                                     + self.odct_cfg['Cfg']['Path']['org_type']
                                     + self.odct_cfg['Cfg']['Path']['org_parent'],
                                     'g'
                                     + self.odct_cfg['Cfg']['Path']['org_type']
                                     + self.odct_cfg['Cfg']['Path']['org_parent']
                                     + self.odct_cfg['Cfg']['Path']['org_ngram'],
                                     self.odct_cfg['Cfg']['Path']['meeting'],
                                     self.odct_cfg['Cfg']['Path']['topic'],
                                     )
        if not os.path.exists(_str_path_out):
            os.makedirs(_str_path_out)
        _str_pathfilename_out = os.path.join(_str_path_out, 'start.txt')
        with codecs.open(_str_pathfilename_out, 'w', 'utf-8') as _file_out:
            # split for <code> section ?
            _file_out.write(str_page_dw)
        return _str_pathfilename_out

    def sele_upload_topic(self, str_fullpathfilename_local):
        _str_fullpathfilename_https = os.path.join('http://',
                                                   self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                   self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                   self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                   + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                   'meet',
                                                   self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                                   self.odct_cfg['Cfg']['Path']['topic'],
                                                   'start'
                                                   )
        _dw_edit = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
        # str_content = _dw_edit.read_content(_str_fullpathfilename_https)
        _dw_edit.update_content(str_fullpathfilename_local,
                                _str_fullpathfilename_https)

    def store_permalink(self):
        # store url for indexing
        if 'Dw_url' not in self.odct_cfg['Cfg']:
            self.odct_cfg['Cfg']['Dw_url'] = []
        _str_url_topic = os.path.join('http://',
                                      self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                      self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                      self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                      + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                      'meet',
                                      self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                      self.odct_cfg['Cfg']['Path']['topic'],
                                      'start',
                                      )
        self.odct_cfg['Cfg']['Dw_url'].append(_str_url_topic)
        return _str_url_topic

    def topic_breadcrumb(self):
        # not used. The breadcrumbs are above content
        '''
        _str_breadcrumb = ''
        _str_breadcrumb += '[[..:..:..:start|Liste des communes]]'
        _str_breadcrumb += ' > '
        _str_breadcrumb += '[[..:..:start|%(name)s]]' % {'name': trigramme_cmn_2_name(odct_cfg['Cfg']['Path']['org'])}
        _str_breadcrumb += ' > '
        _str_breadcrumb += '[[..:start|séance du %(dt_fr)s]]' % {'dt_fr': dt_iso_2_hmn_fr(odct_cfg['Cfg']['Path']['dt'])}
        _str_dw_page_topic += _str_breadcrumb + '\n'
        '''
        return ''

    def topic_title(self, str_title):
        return H(1, str_title)

    def topic_pirate_status(self):
        str_status = ''
        str_status += H(3, 'Cycle de vie')
        str_status += '  * ' + self.odct_cfg['Cfg']['Fiche']['cycle-de-vie'] + '\n'
        return str_status

    def topic_pirate_argumentation(self, odct_argmap=None):
        return Pirate().add_alternative_arguments(odct_argmap)

    def topic_pirate_oi(self, str_title):
        _str_poi = ''

        if 'Tag_filter' not in self.odct_cfg['Cfg']:
            _str_pathfilename_tag_filter = os.path.join(*['.',
                                                          'Lib',
                                                          'filter_de-jure.yaml',
                                                          ]
                                                        )
            with open(_str_pathfilename_tag_filter, 'r') as _file_in:
                _odct_geo_filter = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
            self.odct_cfg['Cfg']['Semantic_de-jure'] = _odct_geo_filter['Cfg']['Semantic_de-jure']
        _str_section_dw_links, _odct_poi_guess = Pirate().guessed_poi(str_title,
                                                                      self.odct_cfg['Cfg']['Semantic_de-jure'],
                                                                      self.odct_cfg
                                                                      )
        # update io pages
        Poi(self.odct_cfg).sele_update_poi_dw_pages(_odct_poi_guess)

        return _str_section_dw_links

    def topic_pirate_tags(self, str_title):
        _str_tag = ''
        if 'Tag_filter' not in self.odct_cfg['Cfg']:
            _str_pathfilename_tag_filter = os.path.join(*['.',
                                                          'Lib',
                                                          'filter_tag.yaml'
                                                          ]
                                                        )
            with open(_str_pathfilename_tag_filter, 'r') as _file_in:
                _odct_tag_filter = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
            self.odct_cfg['Cfg']['Tag_filter'] = _odct_tag_filter['Cfg']['Tag_filter']
        _str_tag = Pirate().guessed_tag(str_title, self.odct_cfg['Cfg']['Tag_filter'])
        # _lst_tag = list(set(_lst_tag)).sort()
        # _str_tag += '{{tag>' + ', '.join(_lst_tag) + ']}}'
        return _str_tag

    def topic_pirate_geo_tags(self, dct_tree_topic, str_title):
        _str_geotag = ''
        if 'Semantic_geo' not in self.odct_cfg['Cfg']:
            _str_pathfilename_tag_filter = os.path.join(*['.',
                                                          'Lib',
                                                          'filter_geo.yaml'
                                                          ]
                                                        )
            with open(_str_pathfilename_tag_filter, 'r') as _file_in:
                _odct_tag_filter = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
            self.odct_cfg['Cfg']['Semantic_geo'] = _odct_tag_filter['Cfg']['Semantic_geo']
        # if _str_title.lower().find('rue') != -1:
        #    print(_str_title)
        _str_section_dw_geo_links, _odct_geo_guess = Pirate().guessed_geoloc(str_title,
                                                                             self.odct_cfg['Cfg']['Semantic_geo'],
                                                                             self.odct_cfg)
        Poi(self.odct_cfg).update_geo_dw_pages(_odct_geo_guess, dct_tree_topic)

        return _str_section_dw_geo_links

    def topic_autority_detail(self, _str_exp=None):
        _str_details = ''
        _str_head_details = H(3, 'Détails') + ''
        if _str_exp is not None:
            _str_details += _str_head_details
            _str_details += _str_exp.lstrip() + '\n'
        elif 'Det' in self.odct_cfg['Cfg']:
            _str_details += _str_head_details + '\n'
            if self.odct_cfg['Cfg']['Det']:
                pass
        else:
            _str_details += CM(_str_head_details).replace('\n', '') + '\n' + '\n'
        return _str_details

    def topic_autority_summary(self, _str_exp=None):
        _str_summary = ''
        _str_head_summary = H(3, 'Résumé') + ''
        if _str_exp is not None:
            _str_summary += _str_head_summary
            _str_summary += _str_exp.lstrip() + '\n'
        elif 'Aut' in self.odct_cfg['Cfg']:
            _str_summary += _str_head_summary + '\n'
            if self.odct_cfg['Cfg']['Aut']:
                pass
        else:
            _str_summary += CM(_str_head_summary).replace('\n', '') + '\n' + '\n'
        return _str_summary

    def topic_autority_note(self, is_decision):
        _str_adm_notes = ''
        if is_decision:
            _str_head_adm_notes = H(3, 'Notes explicatives')
            if 'Aut' in self.odct_cfg['Cfg']:
                _str_adm_notes += _str_head_adm_notes + '\n'
                if self.odct_cfg['Cfg']['Aut']:
                    pass
            else:
                _str_adm_notes += CM(_str_head_adm_notes).replace('\n', '') + '\n' + '\n'
        return _str_adm_notes

    def topic_autority_proposition(self, is_decision):
        _str_proposition = ''
        if is_decision:
            _str_head_proposition = H(3, 'Proposition')
            if 'Aut' in self.odct_cfg['Cfg']:
                _str_proposition += _str_head_proposition + '\n'
                if self.odct_cfg['Cfg']['Aut']:
                    if 'fr' in self.odct_data['Dossier']['Proposition']:
                        _str_proposition += Adm().text_to_dw(self.odct_data['Dossier']['Proposition']['fr'])
                    elif 'nl' in self.odct_data['Dossier']['Proposition']:
                        _str_proposition += Adm().text_to_dw(self.odct_data['Dossier']['Proposition']['nl'])
                    else:
                        _str_proposition += Adm().text_to_dw(self.odct_data['Dossier']['Proposition'])
                else:
                    _str_proposition = CM(_str_head_proposition).replace('\n', '') + '\n' + '\n'
        return _str_proposition

    def topic_autority_deliberation(self):
        _str_deliberation = ''
        _str_head_deliberation = H(3, 'Délibération')
        if 'Aut' in self.odct_cfg['Cfg']:
            _str_deliberation += _str_head_deliberation + '\n'
            if self.odct_cfg['Cfg']['Aut']:
                if 'fr' in self.odct_data['Dossier']['Deliberation']:
                    _str_deliberation += Adm().text_to_dw(self.odct_data['Dossier']['Deliberation']['fr'])
                elif 'nl' in self.odct_data['Dossier']['Deliberation']:
                    _str_deliberation += Adm().text_to_dw(self.odct_data['Dossier']['Deliberation']['nl'])
                else:
                    _str_deliberation += Adm().text_to_dw(self.odct_data['Dossier']['Deliberation'])
        else:
            _str_deliberation += CM(_str_head_deliberation).replace('\n', '') + '\n' + '\n'
        return _str_deliberation

    def topic_autority_decision(self, is_decision):
        _str_decision = ''
        if is_decision:
            _str_head_decision = H(3, 'Décision')
            if 'Aut' in self.odct_cfg['Cfg']:
                _str_decision += _str_head_decision + '\n'
                if self.odct_cfg['Cfg']['Aut']:
                    if 'P-V' in self.odct_data['Dossier']:
                        if 'fr' in self.odct_data['Dossier']['P-V']['decision']:
                            _str_decision += P(self.odct_data['Dossier']['P-V']['decision']['fr'])
                        elif 'nl' in self.odct_data['Dossier']['P-V']['decision']:
                            _str_decision += P(self.odct_data['Dossier']['P-V']['decision']['nl'])
                        else:
                            _str_decision += P(self.odct_data['Dossier']['P-V']['decision'])
            else:
                _str_decision += CM(_str_head_decision).replace('\n', '') + '\n' + '\n'
        return _str_decision

    def topic_autority_finance(self):
        _str_has_finance_impact = ''
        _str_head_has_finance_impact = H(3, 'Impact financier')
        if 'Aut' in self.odct_cfg['Cfg']:
            _str_has_finance_impact += _str_head_has_finance_impact + '\n'
            if self.odct_cfg['Cfg']['Aut']:
                _str_has_finance_impact += self.odct_data['Dossier']['Information']['impact-financier']
        else:
            _str_has_finance_impact += CM(_str_head_has_finance_impact).replace('\n', '') + '\n' + '\n'
        return _str_has_finance_impact

    def topic_autority_votes(self, is_decision):
        _str_votes = ''
        if is_decision:
            _str_head_votes = H(3, 'Votes')
            if 'Aut' in self.odct_cfg['Cfg']:
                _str_votes += _str_head_votes + '\n'
                if self.odct_cfg['Cfg']['Aut']:
                    if 'P-V' in self.odct_data['Dossier']:
                        pass
            else:
                _str_votes += CM(_str_head_votes).replace('\n', '') + '\n' + '\n'
        return _str_votes

    def topic_autority_authentification(self):
        str_authentification = ''
        str_head_authentification = H(3, 'Authentification')
        if 'Aut' in self.odct_cfg['Cfg']:
            str_authentification += str_head_authentification + '\n'
            if self.odct_cfg['Cfg']['Aut']:
                str_authentification += '  * **Service**: %s\n' % self.odct_data['Dossier']['cirb-service']
                if 'cirb_echevin' in self.odct_data['Dossier']:  # 2018-02-23 bug
                    str_authentification += '  * **Echevin**: %s\n' % self.odct_data['Dossier']['cirb_echevin']
                elif 'cirb-echevin' in self.odct_data['Dossier']:
                    str_authentification += '  * **Echevin**: %s\n' % self.odct_data['Dossier']['cirb-echevin']
                str_authentification += '  * **Reunion**: %s\n' % self.odct_data['Dossier']['cirb-reunion']
                if 'cirb-css' in self.odct_data['Dossier']:
                    str_authentification += '  * **Chairman ; Secretary ; Signer (Css)**: %s\n' \
                                            % self.odct_data['Dossier']['cirb-css']
        else:
            str_authentification += CM(str_head_authentification).replace('\n', '') + '\n' + '\n'
        return str_authentification

    def topic_autority_anx(self):
        _str_list = ''
        _str_head_list = H(3, 'Annexes')
        if 'Aut' in self.odct_cfg['Cfg']:
            _str_list += _str_head_list + '\n'
            if self.odct_cfg['Cfg']['Aut']:
                _str_list += CM('Liste des annexes - début')
                _str_list += CM('Liste des annexes - fin')
        else:
            _str_list += CM(_str_head_list).replace('\n', '') + '\n' + '\n'

        return _str_list + '\n'

    def do_topic(self, str_title, lst_exp):
        print(str_title)
        _str_dw_page_topic = ''

        _is_decision = True
        if Pirate().guess_is_information(str_title):
            _is_decision = False

        # store url for indexing
        _str_url_topic = self.store_permalink()

        _dct_tree_topic = {'url': _str_url_topic,
                           'org-name': x_tools.trigramme_cmn_2_name(self.odct_cfg['Cfg']['Path']['org']),
                           'date-time':  x_tools.dt_iso_2_date_hmn_fr(self.odct_cfg['Cfg']['Path']['dt-iso'], with_time=False),
                           'title': str_title,
                           }

        # breadcrumb
        _str_dw_page_topic += self.topic_breadcrumb()

        # Authority topic title
        _str_dw_page_topic += self.topic_title(str_title)

        # Pirate Argumentation
        if _is_decision:
            _str_arg = ''
            if 'Pirate' in self.odct_cfg['Cfg'] and 'add_pirate' in self.odct_cfg['Cfg']['Pirate']:
                if self.odct_cfg['Cfg']['Pirate']['add_pirate']:
                    _str_dw_page_topic += H(2, "Co-construction des Pirates")

                    # Pirate Status
                    _str_dw_page_topic += self.topic_pirate_status()

                    # Pirate Argumentation
                    _str_dw_page_topic += self.topic_pirate_argumentation(odct_argmap=None)

                    # Pirate Evaluations

                    # Pirate Arg_map data
                    _str_dw_page_topic += Pirate().add_argmap_data(self.odct_cfg)

                    # Pïrates divers
                    _str_dw_page_topic += Pirate().info_complementaires()

                    # Pirate Points d'intérêt
                    _str_dw_page_topic += self.topic_pirate_oi(str_title)

                    # Pirate Tag
                    _str_dw_page_topic += self.topic_pirate_tags(str_title)

                    # Pirate GeoTags
                    _str_dw_page_topic += self.topic_pirate_geo_tags(_dct_tree_topic, str_title)

                    # Pirate Semantic
                    _str_dw_page_topic += Pirate().add_semantic(self.odct_cfg)

        _str_dw_page_topic += H(2, ' Documents administratifs') + '\n'

        # Authority Detail
        if lst_exp is not None:
            _str_dw_page_topic += self.topic_autority_detail('\n'.join(lst_exp))
            # print(_str_dw_page_topic)
        else:
            _str_dw_page_topic += self.topic_autority_detail()

        # Authority Summary
        _str_dw_page_topic += self.topic_autority_summary()

        # Authority Notes explicatives
        _str_dw_page_topic += self.topic_autority_note(_is_decision)

        # Authority Proposition
        _str_dw_page_topic += self.topic_autority_proposition(_is_decision)

        # Authority Déliberation
        _str_dw_page_topic += self.topic_autority_deliberation()

        # Authority Decision
        _str_dw_page_topic += self.topic_autority_decision(_is_decision)

        # Authority Impact Financier
        _str_dw_page_topic += self.topic_autority_finance()

        # Authority Votes
        _str_dw_page_topic += self.topic_autority_votes(_is_decision)

        # Authority Autentification
        _str_dw_page_topic += self.topic_autority_authentification()

        # Authority Annexes
        _str_dw_page_topic += self.topic_autority_anx()

        # Short URL
        _str_dw_page_topic += '\n~~SHORTURL~~\n' + '\n'

        # Discussion citoyenne
        _str_dsc = ''
        _str_dsc += '~~DISCUSSION~~'
        _str_dw_page_topic += _str_dsc + '\n'

        _str_fullpathfilename_local = self.save_topic(_str_dw_page_topic)

        # Topic(self.odct_cfg).ftp_upload_topic(_str_fullpathfilename_local)
        Topic(self.odct_cfg).sele_upload_topic(_str_fullpathfilename_local)

        return self.odct_cfg

    def __init__(self, odct_cfg):
        self.odct_cfg = odct_cfg
        '''
        _odct_path['org_parent'] = _lst_org_item[2]  # Bel, Wbr, Bru, ...
        _odct_path['org_ngram'] = _lst_org_item[3]   # Tub, Wtb, Olln, ...
        _odct_path['org_child'] = _lst_org_item[4]   # adm, clg, brg, prs, sec, ...
        '''


class Topics(object):

    def head(self):
        _str_head = ''
        _str_pathfilename_head = os.path.join('.',
                                              'Lib',
                                              'seance_consCmn_head.dw',
                                              )
        if 'Meeting_place' not in self.odct_cfg['Cfg']:
            self.odct_cfg = x_tools.merge_2dicts(self.odct_cfg,
                                                 X_yaml().load_filename_2_odct('./Lib/meeting_place.yaml'))
        with open(_str_pathfilename_head, 'r', encoding='utf-8') as _file_in:
            _str_date = x_tools.dt_iso_2_date_hmn_fr(self.odct_cfg['Cfg']['Path']['dt-iso'], with_time=False)
            _str_datetime = x_tools.dt_iso_2_date_hmn_fr(self.odct_cfg['Cfg']['Path']['dt-iso'], with_time=True)
            _str_orgname = x_tools.trigramme_cmn_2_name(self.odct_cfg['Cfg']['Path']['org'], do_fullname=False)
            _str_meeting_place = self.odct_cfg['Cfg']['Meeting_place']['_'.join([self.odct_cfg['Cfg']['Path']['org_full'],
                                                                                 self.odct_cfg['Cfg']['Path']['meet'],
                                                                                 self.odct_cfg['Cfg']['Path']['grp'],
                                                                                 ])
                                                                       ]
            _str_contact = self.odct_cfg['Cfg']['Fiche']['agent']
            _str_file = _file_in.read()
            _str_head += _str_file % {'contact': _str_contact,
                                      'meeting_place': _str_meeting_place,
                                      'de-jure': _str_orgname,
                                      'date4hmn': _str_date,
                                      'datetime4hmn': _str_datetime,
                                      }
        return _str_head

    def save_local(self, str_page_dw):
        _str_path_out = os.path.join('..',
                                     'Data',
                                     'Archives',
                                     'g'
                                     + self.odct_cfg['Cfg']['Path']['org_type']
                                     + self.odct_cfg['Cfg']['Path']['org_parent'],
                                     'g'
                                     + self.odct_cfg['Cfg']['Path']['org_type']
                                     + self.odct_cfg['Cfg']['Path']['org_parent']
                                     + self.odct_cfg['Cfg']['Path']['org_ngram'],
                                     self.odct_cfg['Cfg']['Path']['meeting']
                                     )
        if not os.path.exists(_str_path_out):
            os.makedirs(_str_path_out)
        _str_pathfilename_out = os.path.join(_str_path_out, 'start.txt')
        with codecs.open(_str_pathfilename_out, 'w', 'utf-8') as _file_out:
            _file_out.write(str_page_dw)
        return _str_pathfilename_out

    def sele_upload_topics_media(self):
        _str_pathfilename_topics_media = os.path.abspath(os.path.join('..',
                                                                      'Data',
                                                                      self.odct_cfg['Cfg']['Path']['filename_src']
                                                                      )
                                                         )
        _str_https_fullpathfilename_topics_media = os.path.join('https://',
                                                                self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                                '_media',
                                                                self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                                self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                                + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                                'meet',
                                                                self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                                                self.odct_cfg['Cfg']['Path']['filename_src']
                                                                )
        _str_https_fullpathfilename_topics_page = os.path.join('https://',
                                                               self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                               self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                               self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                               + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                               'meet',
                                                               self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                                               'start'
                                                               )
        _dw_edit = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
        _dw_edit.save_media(_str_pathfilename_topics_media,
                            _str_https_fullpathfilename_topics_page
                            )

        return _str_https_fullpathfilename_topics_media

    def sele_upload_topics_page(self, str_content):
        _str_https_fullpathfilename_topics = os.path.join('https://',
                                                          self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                          self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                          self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                          + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                          'meet',
                                                          self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                                          'start'
                                                          )
        _dw_edit = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
        _dw_edit.save_content(_str_https_fullpathfilename_topics, str_content)

    def split_page(self, str_page, str_flag_begin, str_flag_end):
        _lst_line = str_page.split('\n')
        _lst_head = []
        _lst_list = []
        _lst_tail = []
        _is_head = True
        _is_tail = False
        for _str_line in _lst_line:
            # remove last char ?
            if _is_head:
                if _str_line.startswith('/* ' + str_flag_begin):
                    _lst_head.append(_str_line)
                    _is_head = False
                    continue
                _lst_head.append(_str_line)
            elif _is_tail:
                _lst_tail.append(_str_line)
            else:
                if _str_line.startswith('/* ' + str_flag_end):
                    _lst_tail.append(_str_line)
                    _is_tail = True
                    continue
                _lst_list.append(_str_line)
        return _lst_head, _lst_list, _lst_tail

    def create_new_org(self):
        _str_page = ''
        _str_ngram = self.odct_cfg['Cfg']['Path']['org']
        _str_page += H(1, self.odct_cfg['Cfg']['N-gram'][_str_ngram]['class_and_name'].title())
        _str_page += UL(A('.:meet:start', 'réunions'))
        _str_page += CM('Reunions begin').replace('\n', '') + '\n'
        _str_page += CM('Reunions end').replace('\n', '') + '\n'
        _str_page += H(2, 'Partenaires')
        _str_page += CM('Partenaires begin').replace('\n', '') + '\n'
        _str_page += CM('Partenaires end').replace('\n', '') + '\n'
        _str_page += H(2, 'Documents')
        _str_page += CM('Documents begin').replace('\n', '') + '\n'
        _str_page += CM('Documents end').replace('\n', '') + '\n'
        # print(_str_page)
        return _str_page

    def update_org(self):
        # just create if not exist
        _str_https_fullpathfilename_org = os.path.join('https://',
                                                       self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                       self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                       self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                       + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                       'start'
                                                       )
        # already exists ?
        _str_page_old = ''
        _dw_edit = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
        _str_content = _dw_edit.read_content(_str_https_fullpathfilename_org)
        if _str_content == '':
            _str_content = self.create_new_org()
            _dw_edit.save_content(_str_https_fullpathfilename_org, _str_content)

    def str_link_meeting_label(self):
        _str_ngram = self.odct_cfg['Cfg']['Path']['org']
        _str_link_label = self.odct_cfg['Cfg']['N-gram'][_str_ngram]['class_and_name']
        _str_link_label = _str_link_label[0].upper() + _str_link_label[1:]
        _str_link_label += ' - conseil communal - '
        _str_link_label += 'séance du %s' % x_tools.dt_iso_2_date_hmn_fr(self.odct_cfg['Cfg']['Path']['dt-iso'])
        return _str_link_label

    def str_remote_pathfilename_meeting(self):
        _str_link_adr = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                     self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                     self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                     + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                     'meet',
                                     self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                     'start'
                                     )
        return _str_link_adr

    def page_insert_item(self, str_fullpathfilename_page, str_list_item, create_difussion_proc, str_flag_begin, str_flag_end):

        _str_page_old = ''
        _dw_edit = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
        _str_page_old = _dw_edit.read_content('https://' + str_fullpathfilename_page)
        if _str_page_old == '':
            _str_page_old = create_difussion_proc
        _lst_head, _lst_list, _lst_tail = self.split_page(_str_page_old, str_flag_begin, str_flag_end)
        _lst_list.append(str_list_item)
        _lst_list = list(filter(None, _lst_list))
        _lst_list = list(set(_lst_list))
        _lst_list.sort(reverse=True)
        _str_content = '\n'.join(_lst_head) + '\n' \
            + '\n'.join(_lst_list) + '\n' + '\n' \
            + '\n'.join(_lst_tail)
        # print(_str_content)
        if _str_content == _str_page_old:
            _selm_cancel = self.odct_cfg['Cfg']['Sele'].driver.find_element_by_xpath("//button[@name='do[draftdel]']")
            # _selm_cancel = self.odct_cfg['Cfg']['Sele'].driver.find_element_by_xpath("//button[contains(@id, 'draftdel')]")
            _selm_cancel.send_keys(Keys.ENTER)
            time.sleep(1)
        else:
            _dw_edit.save_content('https://' + str_fullpathfilename_page, _str_content)

    def update_home_page(self):
        _str_fullpathfilename_page = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                  'start'
                                                  )
        _str_fullpathfilename_meeting = self.str_remote_pathfilename_meeting()
        _str_commonpath = os.path.commonpath([_str_fullpathfilename_page, _str_fullpathfilename_meeting])
        _str_pathfilename_meeting = _str_fullpathfilename_meeting.replace(_str_commonpath, '.')
        _str_list_item = '  * /* %(key)s */ [[%(adr)s|%(label)s]]' \
                         % {'key': self.odct_cfg['Cfg']['Path']['dt-iso'],
                            'adr': _str_pathfilename_meeting,
                            'label': self.str_link_meeting_label()
                            }
        self.page_insert_item(_str_fullpathfilename_page,
                              _str_list_item,
                              None,
                              'News begin',
                              'News end')

    def create_new_org_meet(self):
        _str_page = ''
        _str_ngram = self.odct_cfg['Cfg']['Path']['org']
        _str_page += H(1, 'Reunions')
        _str_page += H(2, self.odct_cfg['Cfg']['N-gram'][_str_ngram]['class_and_name'].title())
        _str_page += H(3, 'Conseil communal')
        _str_page += CM('Reunions consCmn begin').replace('\n', '') + '\n'
        _str_page += CM('Reunions consCmn end').replace('\n', '') + '\n'
        # print(_str_page)
        return _str_page

    def update_org_meet(self):
        _str_fullpathfilename_page = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                  self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                  self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                  + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                  'meet',
                                                  'start'
                                                  )
        _str_fullpathfilename_meeting = self.str_remote_pathfilename_meeting()
        _str_commonpath = os.path.commonpath([_str_fullpathfilename_page, _str_fullpathfilename_meeting])
        _str_pathfilename_meeting = _str_fullpathfilename_meeting.replace(_str_commonpath, '.')
        _str_list_item = '  * /* %(key)s */ [[%(adr)s|%(label)s]]' \
                         % {'key': self.odct_cfg['Cfg']['Path']['dt-iso'],
                            'adr': _str_pathfilename_meeting,
                            'label': self.str_link_meeting_label()
                            }
        self.page_insert_item(_str_fullpathfilename_page,
                              _str_list_item,
                              self.create_new_org_meet(),
                              'Reunions consCmn begin',
                              'Reunions consCmn end')

    def create_news(self):
        _str_page = ''
        _str_ngram = self.odct_cfg['Cfg']['Path']['org']
        _str_page += H(1, 'News')
        _str_page += CM('News begin').replace('\n', '') + '\n'
        _str_page += CM('News end').replace('\n', '') + '\n'
        # print(_str_page)
        return _str_page

    def update_news(self):
        _str_fullpathfilename_page = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                  'news',
                                                  'start'
                                                  )
        _str_fullpathfilename_meeting = self.str_remote_pathfilename_meeting()
        _str_commonpath = os.path.commonpath([_str_fullpathfilename_page, _str_fullpathfilename_meeting])
        _str_pathfilename_meeting = _str_fullpathfilename_meeting.replace(_str_commonpath, '..')
        _str_list_item = '  * /* %(key)s */ [[%(adr)s|%(label)s]]' \
                         % {'key': self.odct_cfg['Cfg']['Path']['dt-iso'],
                            'adr': _str_pathfilename_meeting,
                            'label': self.str_link_meeting_label()
                            }
        self.page_insert_item(_str_fullpathfilename_page,
                              _str_list_item,
                              self.create_news(),
                              'News begin',
                              'News end')

    def create_diffussion(self):
        _str_page = ''
        _str_ngram = self.odct_cfg['Cfg']['Path']['org']
        _str_page += H(1, 'Diffusion')
        _str_page += CM('Diffusion begin').replace('\n', '') + '\n'
        _str_page += CM('Diffusion end').replace('\n', '') + '\n'
        # print(_str_page)
        return _str_page

    def update_diffusion(self):
        _str_fullpathfilename_page = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                  'diffusion',
                                                  'start'
                                                  )
        _str_fullpathfilename_meeting = self.str_remote_pathfilename_meeting()
        _str_commonpath = os.path.commonpath([_str_fullpathfilename_page, _str_fullpathfilename_meeting])
        _str_pathfilename_meeting = _str_fullpathfilename_meeting.replace(_str_commonpath, '..')
        _str_list_item = '  * /* %(key)s */ [[%(adr)s|%(label)s]]' \
                         % {'key': self.odct_cfg['Cfg']['Path']['dt-iso'],
                            'adr': _str_pathfilename_meeting,
                            'label': self.str_link_meeting_label()
                            }
        self.page_insert_item(_str_fullpathfilename_page,
                              _str_list_item,
                              self.create_diffussion(),
                              'Diffusion begin',
                              'Diffusion end')

    def create_public_share(self):
        _str_page = ''
        _str_ngram = self.odct_cfg['Cfg']['Path']['org']
        _str_page += H(1, 'Public share')
        _str_page += CM('4Public begin').replace('\n', '') + '\n'
        _str_page += CM('4Public end').replace('\n', '') + '\n'
        # print(_str_page)
        return _str_page

    def update_public_share(self):
        _str_https_fullpathfilename_topics_media = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                                '_media',
                                                                self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                                self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                                + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                                'meet',
                                                                self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                                                self.odct_cfg['Cfg']['Path']['filename_src']
                                                                )
        _str_https_fullpathfilename_topics_page = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                               self.odct_cfg['Cfg']['Path']['org_parent'].lower(),
                                                               self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                               + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                               'meet',
                                                               self.odct_cfg['Cfg']['Path']['meeting'].lower(),
                                                               'start'
                                                               )
        _str_list_item = '|%(media)s|%(page)s| ' % {'media': 'https://' + _str_https_fullpathfilename_topics_media,
                                                    'page': 'https://' + _str_https_fullpathfilename_topics_page,
                                                    }

        _str_https_fullpathfilename = os.path.join(self.odct_cfg['Cfg']['Remote']['Dw']['domain'],
                                                   'public_share',
                                                   'start'
                                                   )
        self.page_insert_item(_str_https_fullpathfilename,
                              _str_list_item,
                              self.create_public_share(),
                              '4Public begin',
                              '4Public end')

    def do_topics(self, str_doc_src):

        _str_dw_page = ''
        _str_dw_page += self.head()
        _str_dw_page += H(2, 'Séance publique') + '\n'

        _int_size_exp = 0
        _lst_line = str_doc_src.splitlines()
        _int_lines = len(_lst_line)
        for _int_pos_line, _str_line in enumerate(_lst_line):

            # skip line with notes
            if _int_size_exp != 0:
                _int_size_exp -= 1
                continue

            # title ?
            if _str_line.startswith('#'):
                _int_rank = _str_line.count('#')
                _str_dw_page += H(2 + _int_rank, _str_line)
                continue
            self.odct_cfg['Cfg']['Path']['topic_pos'] = _int_pos_line + 1
            self.odct_cfg['Cfg']['Path']['topic'] = 'sp%(pos)03i' % {'pos': _int_pos_line + 1}

            # has details ?
            _lst_exp = None
            _int_size_exp = 0
            while (_int_pos_line + 1 + _int_size_exp) != _int_lines \
                    and _lst_line[_int_pos_line + 1 + _int_size_exp].startswith('  '):
                print(_int_pos_line, _int_size_exp, _lst_line[_int_pos_line + 1 + _int_size_exp])
                if _lst_exp is None:
                    _lst_exp = []
                _lst_exp.append(_lst_line[_int_pos_line + 1 + _int_size_exp])
                _int_size_exp += 1

            _dct_paths = Topic(self.odct_cfg).do_topic(_str_line, _lst_exp)
            _str_anchor = '[[.:%(link)s:start|%(label)s]]' % {'link': self.odct_cfg['Cfg']['Path']['topic'],
                                                              'label': _str_line,
                                                              }
            _str_dw_page += '  * ' + _str_anchor + '\n'
            if 'Pirate' in self.odct_cfg['Cfg'] and 'add_pirate' in self.odct_cfg['Cfg']['Pirate']:
                if self.odct_cfg['Cfg']['Pirate']['add_pirate']:
                    if not Pirate().guess_is_information(_str_line):
                        # print(_dct_topic)
                        _str_dw_page += Pirate().add_pool(self.odct_cfg['Cfg']['Path']['dt-iso'],
                                                          self.odct_cfg['Cfg']['Path']['org_parent'].lower()
                                                          + self.odct_cfg['Cfg']['Path']['org_type'].lower()
                                                          + self.odct_cfg['Cfg']['Path']['org_ngram'].lower(),
                                                          'conseil',
                                                          self.odct_cfg['Cfg']['Path']['topic'],
                                                          )
            # print(_str_dw_page)
            # break  # debug just pass one topic

        # LD ?

        # Src
        _str_dw_page += H(3, 'Source(s)') + '\n'
        _str_dw_page += CM('Début pour diffusion publique')
        _str_https_fullpathfilename_topics_media = self.sele_upload_topics_media()
        _str_url_dw_media = '  * {{%(url)s|%(name)s}}' % {'url': _str_https_fullpathfilename_topics_media,
                                                          'name': self.odct_cfg['Cfg']['Path']['filename_src']
                                                          }
        _str_dw_page += _str_url_dw_media
        _str_dw_page += '\n'
        _str_dw_page += CM('Fin pour diffusion publique')
        # _dw_upload_media = Dw_edit(self.odct_cfg['Cfg']['Sele'], self.odct_cfg['Cfg']['Remote']['Dw'])
        # self.sele_upload_topics_media()

        # Short URL
        _str_dw_page += '\n~~SHORTURL~~' + '\n'

        # Discussion citoyenne
        _str_dsc = ''
        _str_dsc += '~~DISCUSSION~~'
        _str_dw_page += _str_dsc + '\n'

        self.save_local(_str_dw_page)

        self.sele_upload_topics_page(_str_dw_page)

        self.update_org_meet()
        self.update_org()
        self.update_home_page()

        self.update_diffusion()
        self.update_public_share()
        self.update_news()

        return self.odct_cfg

    def __init__(self, odct_cfg):
        self.odct_cfg = odct_cfg


class Conseil_citoyen(object):

    def filename_2_dct_path(self, str_filename_src):
        _odct_path = collections.OrderedDict()
        _odct_path['filename_src'] = str_filename_src
        _odct_path['meeting'] = '_'.join(os.path.splitext(str_filename_src)[0].split('_')[:-1])
        _odct_path['docname_src'] = '_'.join(os.path.splitext(str_filename_src)[0].split('_')[:-1]) \
                                    + '_' + os.path.splitext(str_filename_src)[0].split('_')[-1].split('-')[0]
        _odct_path['docname_lst'] = _odct_path['docname_src'] + '.lst'
        _odct_path['meeting'] = '_'.join(os.path.splitext(str_filename_src)[0].split('_')[:4])
        _odct_path['base'] = '.'
        _lst_under = os.path.splitext(str_filename_src)[0].split('_')
        _odct_path['dt-iso'] = _lst_under[0]
        _odct_path['org_full'] = _lst_under[1]
        _odct_path['meet'] = _lst_under[2]
        _odct_path['grp'] = _lst_under[3]
        _odct_path['doc_type'] = _lst_under[4]
        _odct_path['topic'] = ''
        _lst_org_item = x_tools.mixedCase_2_list(_odct_path['org_full'])
        _odct_path['org'] = ''.join(_lst_org_item[:4])
        _odct_path['org_type'] = _lst_org_item[1]    # Cmn, Prv, Zp, Rgn, Cmt, Fed
        _odct_path['org_parent'] = _lst_org_item[2]  # Bel, Wbr, Bru, ...
        _odct_path['org_ngram'] = _lst_org_item[3]   # Tub, Wtb, Olln, ...
        _odct_path['org_child'] = _lst_org_item[4]   # adm, clg, brg, prs, sec, ...
        return _odct_path

    def __init__(self, odct_cfg):
        self.odct_cfg = odct_cfg


def first_use(self):

    _str_path_config = '../Config'

    if not os.path.exists(_str_path_config):
        os.mkdir(_str_path_config)

        _str_path_data = '../Data'
        if not os.path.exists(_str_path_data):
            os.mkdir(_str_path_data)

        _str_path_template = './Templates'

        # main
        _str_filename = str_prg_name + '.yaml'
        _str_pathfilename_template = os.path.join(_str_path_template, _str_filename)
        _str_pathfilename_config = os.path.join(_str_path_config, _str_filename)
        shutil.copyfile(_str_pathfilename_template, _str_pathfilename_config)

        # main private
        _str_filename = str_prg_name + '_private.yaml'
        _str_pathfilename_template = os.path.join(_str_path_template, _str_filename)
        _str_pathfilename_config = os.path.join(_str_path_config, _str_filename)
        shutil.copyfile(_str_pathfilename_template, _str_pathfilename_config)


def odj_lst_2_dw():

    if not os.path.exists('../Data'):
        first_use()

    str_prg_name = os.path.split(sys.argv[0])[1]
    _x_config = x_tools.X_config()
    _odct_cfg = collections.OrderedDict()
    _odct_cfg = x_tools.merge_2dicts(_odct_cfg, _x_config.load_config(str_prg_name))
    # _odct_cfg = merge_2dicts(_odct_cfg, _x_config.load_config(os.path.splitext(str_prg_name)[0] + '_debug'))

    # load the metadata of list
    _str_pathfilename_doc_cfg = os.path.join(*['.',
                                               'Lib',
                                               'ngram' + '.yaml'
                                               ]
                                             )
    # print(os.path.abspath(_str_pathfilename_doc_cfg))
    with open(_str_pathfilename_doc_cfg, 'r') as _file_in:
        _cfg_ngram = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
    _odct_cfg = x_tools.merge_2dicts(_odct_cfg, _cfg_ngram)

    if 'Todo' in _odct_cfg['Cfg']:
        for _str_org in _odct_cfg['Cfg']['Todo']:
            _odct_cfg = x_tools.merge_2dicts(_odct_cfg, _x_config.load_config(_str_org))

            # Refresh Selenium for each document
            _odct_cfg['Cfg']['Sele'] = X_selenium(_odct_cfg, 'Cfg')

            _odct_cfg['Cfg']['Path'] = Conseil_citoyen(_odct_cfg).filename_2_dct_path(_odct_cfg['Cfg']['Todo'][0])

            # List of topics
            _lst_pathfilename_in = ['..',
                                    'Data',
                                    _odct_cfg['Cfg']['Path']['docname_lst']]
            # _str_pathfilename_in = os.path.join(*_lst_pathfilename_in)
            _str_in = x_tools.read_file(_lst_pathfilename_in)

            # Metadata 'fiche' of odj_lst
            _str_filename_doc_cfg = _odct_cfg['Cfg']['Path']['docname_src'] + '.yaml'
            _str_pathfilename_doc_cfg = os.path.join(*['..',
                                                       'Data',
                                                       _str_filename_doc_cfg
                                                       ]
                                                     )
            with open(_str_pathfilename_doc_cfg, 'r') as _file_in:
                _cfg_doc = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
            _odct_cfg = x_tools.merge_2dicts(_odct_cfg, _cfg_doc)

            _odct_cfg = Topics(_odct_cfg).do_topics(_str_in)

            _str_path_base = '../Data'
            _str_path_out = os.path.join('..',
                                         'Data',
                                         'Archives',
                                         'g'
                                         + _odct_cfg['Cfg']['Path']['org_type']
                                         + _odct_cfg['Cfg']['Path']['org_parent'],
                                         'g'
                                         + _odct_cfg['Cfg']['Path']['org_type']
                                         + _odct_cfg['Cfg']['Path']['org_parent']
                                         + _odct_cfg['Cfg']['Path']['org_ngram'],
                                         _odct_cfg['Cfg']['Path']['meeting']
                                         )
            for _str_entry in os.listdir(_str_path_base):
                _str_path_entry = os.path.join(_str_path_base, _str_entry)
                if os.path.isfile(_str_path_entry):
                    _str_pathfilename_dst = os.path.join(_str_path_out, _str_entry)
                    shutil.move(_str_path_entry, _str_pathfilename_dst)


def main():
    odj_lst_2_dw()


if __name__ == '__main__':
    _dt_now_begin = datetime.datetime.now()
    str_prg_name = os.path.split(sys.argv[0])[1]
    print('Begin %s ' % (str_prg_name + ' ' + _dt_now_begin.strftime('%Y-%m-%d %H:%M:%S')))
    main()
    _dt_now_end = datetime.datetime.now()
    print('End %s ' % (str_prg_name + ' ' + _dt_now_end.strftime('%Y-%m-%d %H:%M:%S')))
    _dt_delta = _dt_now_end - _dt_now_begin
    print('Delta %s' % str(_dt_delta))
