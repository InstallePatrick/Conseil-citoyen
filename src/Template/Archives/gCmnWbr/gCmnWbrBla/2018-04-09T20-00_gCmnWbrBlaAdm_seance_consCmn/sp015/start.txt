
====== 8. Budget communal 2018 - modification budgetaire n°1 des services ordinaire et extraordinaire ======

===== Co-construction des Pirates =====

==== Cycle de vie ====
  * 20018-03-28 acquisition


==== Argumentation ====

[[https://wikicitoyen.be/faq/start#ou_discuter_a_propos_du_wikicitoyen|Aide]]

/* begin of arguments */
  * **<color /yellow> Pour </color>**
    * Parce que <color /yellow>j'appuie le collège</color>
    * Parce que <color /yellow>le dossier est complet et convaincant</color>
  * **<color /lightcyan> Contre </color>**
    * Parce que <color /lightcyan>je suis contre le collège</color>
    * Parce que <color /lightcyan>l'information est absente ou insuffisante</color>
    * Parce que <color /lightcyan>la transparence administrative n'a pas été respectée</color>
  * **<color /mistyrose> Abstention </color>**
    * Parce que <color /mistyrose>justification reprise au procès-verbal</color>
    * Parce que <color /mistyrose>le quorum n'est pas atteint</color>

/* end of arguments */

==== Evaluation ====

Est-ce que les arguments sont vrais, pertinents et importants ?

/* begin of evaluations */
^ Aternative ^ Critère ^ Evaluation de X|
| <color /yellow>Pour</color>| <color /yellow>j'appuie le collège</color> | Vrai - <color lightgreen> peu pertinent</color> - <color lightpink> peu important </color>|
| | <color /yellow>le dossier est complet et convaincant</color> | Vrai - <color darkgreen> tres pertinent</color> - <color darkred> très important </color>|
| <color /lightcyan>Contre</color>| <color /lightcyan>je suis contre le collège</color> | Vrai - <color lightgreen> peu pertinent </color> - <color lightpink>  peu important  </color>|
| | <color /lightcyan>l'information est absente ou insuffisante </color> | Vrai - <color green> pertinent </color> - <color red>  important  </color>|
| | <color /lightcyan>la transparence administrative n'a pas été respectée </color> | Vrai - <color green> pertinent </color> - <color red>  important  </color>|
| <color /mistyrose>Abstention</color>| <color /mistyrose>la justification doit être reprise au procès-perbal </color> | Vrai - <color green> pertinent </color> - <color red>  important  </color> |
| | <color /mistyrose>le quorum n'est pas atteint</color> | Faux (pas de vote sans quorum) |
| | ^ Conclusion ??? |

/* end of evaluations */


<hidden Carte d'arguments>
<code yaml>
ArgMap:
  @context:
    schema: http://schema.org/
  Alt:
  - value: dummy
    Args: 
    - value: dummy too

</code>
</hidden>


==== Informations complémentaires ====

Certaines informations n'appuient pas un choix mais méritent d'être publiées
/* ===== Points d'intérêt ===== */


/* begin poi */

/* end poi */

===== Tags =====


/* begin of tag list */
{{tag>}}

/* end of tag list */
/* ===== Geolocalisation ===== */


/* begin geo */

/* end geo */

<hidden Sémantique>
<code yaml>
Semantic:
  @context:
    schema: http://schema.org/ 
</code>
</hidden>

=====  Documents administratifs =====


==== Détails ====
Il est proposé au Conseil communal d'arrêter la modification budgétaire n°1 du budget communal de l'exercice 2018, relative aux services ordinaire et extraordinaire.
/* ==== Résumé ==== */

/* ==== Notes explicatives ==== */

/* ==== Délibération ==== */

/* ==== Décision ==== */

/* ==== Impact financier ==== */

/* ==== Votes ==== */

/* ==== Authentification ==== */

/* ==== Annexes ==== */



~~SHORTURL~~

~~DISCUSSION~~
