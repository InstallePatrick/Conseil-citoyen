====== Braine-l'Alleud, conseil communal du lundi 09 avril 2018 ======

Les séances des [[https://fr.wikipedia.org/wiki/Conseil_communal_(Belgique)|conseils communaux]] sont le lieu où se décide le vivre ensemble dans le cadres des [[https://www.belgium.be/fr/la_belgique/pouvoirs_publics/communes/competences|compétences communales]].\\
Lors des séances publiques des conseils communaux les citoyens (vous) ne peuvent pas s'exprimer directement.\\
Afin d'informer les citoyens, les points à l'ordre du jour sont affichés 7 jours francs avant la réunion (minimum 10 séance par an).\\
Les citoyens ont donc au minimum 7 jours pour contacter les élus et faire part de leurs remarques.\\
[[https://wiki.pirateparty.be/Main_Page|Les pirates]] veulent la transparence et agissent pour l'accès libre aux informations publiques.

Envie d'être averti des mises à jours ? Envoyez un courriel à conseilcitoyen@P-Installe.Be
----
====== Co-construction Pirate ======
**CONSEIL COMMUNAL** \\
Séance du lundi 09 avril 2018 à 20h00 \\
salle du Conseil, [[http://goo.gl/maps/Z0mJV|Grand-Place Baudouin Ier, 3 (2e étage) à 1420 Braine-l'Alleud]]


__ORDRE DU JOUR__


===== Séance publique =====

  * [[.:sp001:start|1. Intercommunale de mutualisation en matiere informatique et organisationnelle (IMIO) - renouvellement des organes de gestion - designation d'un administrateur au sein du conseil d'administration]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp001>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp003:start|2. Plan de cohesion sociale - rapport financier portant sur l'année 2017]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp003>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp005:start|3. Zone de police de Braine-l’alleud n° 5273 - declaration de la vacance d'emplois - mobilite 2018-02]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp005>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp007:start|4. Directeur financier - rapport annuel au conseil communal sur l'execution de sa mission]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp007>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp009:start|5. Subsides - rapport annuel au conseil - annee 2017]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp009>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp011:start|6. Proces-verbal de verification de la caisse communale au 31.12.2017]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp011>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp013:start|7. Comptes communaux 2017]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp013>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp015:start|8. Budget communal 2018 - modification budgetaire n°1 des services ordinaire et extraordinaire]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp015>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp017:start|9. Zone de police n° 5273 - comptes annuels 2017]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp017>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp019:start|10. Zone de police n° 5273 - proces-verbal de verification de la caisse au 31.12.2017]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp019>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp021:start|11. Transfert a la zone de secours du brabant wallon des emprunts contractes par la commune relatifs a la caserne de mont-saint-jean reprise par ladite zone de secours]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp021>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp023:start|12. Zone de police de Braine-l’alleud n° 5273 - budget extraordinaire 2018 - mode de passation et conditions de certains marches]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp023>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp025:start|13. Fabrique d'église saint-étienne - comptes 2017 - approbation avec remarques]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp025>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp027:start|14. Fabrique d'église notre-dame du bon conseil - comptes 2017- avis defavorable]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp027>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp029:start|15. Fabrique d'église saint-sébastien - comptes 2017 - approbation]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp029>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp031:start|16. Creation d'un comité de pilotage pour l'élaboration et le suivi du plan d'action en faveur de l'energie durable - projet pollec 3]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp031>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp033:start|17. Eclairage public - renforcement du reseau ep - eclairage des passages pour pietons a la route de piraumont - 3 points]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp033>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp035:start|18. Remplacement eclairage public dans le centre-ville - phase 1 - partie]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp035>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp037:start|19. Amenagement reseau cyclable a l'initiative de la province du brabant wallon - chemin de baulers et chemin du champ - conventions fixant les modalites d'execution des travaux a charge de la province du brabant wallon]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp037>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp039:start|20. Plan d'alignement - decret du 06.02.2014 relatif a la voirie communale - approbation du projet de modification des plans d'alignements du chemin n° 114 et du sentier n° 114 - approbation provisoire et mise a enquete publique]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp039>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp041:start|21. Plan d'alignement - decret du 06.02.2014 relatif a la voirie communale - approbation du projet de regularisation du plan d'alignement modificatif de l'angle forme par la rue cloquet et la rue de la goëtte - approbation definitive du plan d'alignement modificatif]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp041>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp043:start|22. Plan d'alignement - decret du 06.02.2014 relatif a la voirie communale - approbation du projet de regularisation du plan d'alignement modificatif de l'angle forme par la rue saint-sébastien et l'avenue alphonse allard - approbation definitive du plan d'alignement modificatif]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp043>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp045:start|23. R.F.I. - comptes annuels et etat des recettes et depenses relatifs a l'exercice 2017 - approbation provisoire]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp045>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp047:start|24. Motion du conseil communal concernant le projet de loi autorisant les visites domiciliaires - proposition]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp047>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp049:start|25. Motion "Braine-l'alleud, commune hospitaliere"]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp049>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp051:start|26. Proces-verbal de la seance publique du 26.02.2018]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp051>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp052:start|27. Proces-verbal de la seance publique du 26.03.2018]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp052>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>

  * [[.:sp053:start|28. Questions diverses (article 79 du règlement d'ordre intérieur)]]

<hidden Votes citoyens><poll 2018-04-09T20-00_wbrcmnbla_conseil-sp053>Votre opinion !
  * Pour
  * Contre
  * Abstention
</poll>

</hidden>


==== Source(s) ====


/* Début pour diffusion publique */
  * {{https://www.conseilcitoyen.be/_media/wbr/cmnbla/meet/2018-04-09t20-00_gcmnwbrblaadm_seance_conscmn/2018-04-09T20-00_gCmnWbrBlaAdm_seance_consCmn_odj-src.html|2018-04-09T20-00_gCmnWbrBlaAdm_seance_consCmn_odj-src.html}}

/* Fin pour diffusion publique */

~~SHORTURL~~
~~DISCUSSION~~
