'''
Created on 14 févr. 2018

@author: patrick
'''
'''
Created on 7 déc. 2016

@author: patrick
'''


def H(int_level=1, _str=''):
    '''
    Headings
    '''
    return "\n==" + '='*(5-int_level) + ' ' + str(_str) + ' ' + '='*(5-int_level) + "==" + '\n'


def H1(_str):
    ''' Heading 1 (obsolete) '''
    return "\n====== " + str(_str) + " ======"


def H2(_str):
    ''' Heading 2 (obsolete) '''
    return "\n===== " + str(_str) + " ====="


def H3(_str):
    ''' Heading 3 (obsolete) '''
    return "\n==== " + str(_str) + " ===="


def H4(_str):
    ''' Heading 4 (obsolete) '''
    return "\n=== " + str(_str) + " ==="


def H5(_str):
    ''' Heading 5 (obsolete) '''
    return "\n== " + str(_str) + " =="


def P(_str):
    ''' Paragraph '''
    return "\n" + str(_str) + "\n"


def NL():
    ''' Newline '''
    return "\n"


def HR():
    ''' Horizontal rule '''
    return "\n" + '----' + ""


def B(_str):
    ''' Bold '''
    return "**" + str(_str) + "**"


def U(_str):
    ''' underline '''
    return "__" + str(_str) + "__"


def I(_str):
    ''' Italic '''
    return "//" + str(_str) + "//"


def TABLE(_str):
    ''' Table '''
    return str(_str) + "\n"


def TR(_str):
    ''' Table row '''
    return str(_str) + '|'


def TH(_str):
    ''' Table cell as head '''
    return "^" + str(_str)


def TD(_str):
    ''' Table daté '''
    return "|" + str(_str)


def UL(_str, int_level=1):
    ''' Unnumbered list '''
    return '  ' * int_level + '* ' + str(_str)


def OL(_str):
    ''' Ordered list '''
    return "  - " + str(_str)


def LINK(_str_adr, _str_label):
    ''' Link '''
    return "[[%(adr)s|%(lbl)s]]" % {'adr': _str_adr, 'lbl': _str_label}

def A(_str_adr, _str_label):
    ''' Link '''
    return "[[%(adr)s|%(lbl)s]]" % {'adr': _str_adr, 'lbl': _str_label}

def CM(_str):
    ''' comment '''
    return "\n/* " + str(_str) + " */\n"
     