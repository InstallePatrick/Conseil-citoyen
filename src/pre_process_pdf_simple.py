"""


The title are marked by # ## ### #### as in markdown

"""
pkgname = 'monitor_web'

__author__ = 'Installé Patrick <PatrickInstalle@P-Installe.be>'
__copyright__ = 'Copyright 2018 Patrick Installé'
__homepage__ = ''
__version__ = 'a0.001'

import sys
import os
import datetime
import yaml

import subprocess


str_odct_flags = '''
Flags:
  Start:
  - Séance publique
  - Seance publiqeu
  End:
  - Séance à huis clos
  - Séance à huis-clos
  - Seance a huis clos
  - Seance a huis-clos
  - Huis clos
  - Huis-clos
'''

def merge_lines_non_digit(lst_line):
    _lst_line_out = []
    for _int_pos, str_line in enumerate(lst_line):
        if str_line[0].isdigit():
            _lst_line_out.append(str_line)
        else:
            _lst_line_out[-1] += ' ' + str_line
    return _lst_line_out

def indent_content(lst_line):
    _lst_line_out = []
    for _int_pos, str_line in enumerate(lst_line):
        if str_line.find('. ', 5) != -1:
            _lst_split = str_line[5:].split('. ',1)
            _lst_line_out.append(str_line[:5] +  _lst_split[0] + '.')
            _lst_line_out.append('  ' + ''.join(_lst_split[1:]))
        else:
            _lst_line_out.append(str_line)
    return _lst_line_out

def extract(_str_pathfilename_pdf):
    _odct_flags = yaml.load(str_odct_flags) # Loader=yaml_orderedDict.OrderedLoader
    
    # source is .pdf file
    subprocess.call(['/usr/bin/pdftotext', '-enc', 'UTF-8', '-eol', 'unix', '-layout', _str_pathfilename_pdf])
    
    # process txt
    _lst_line_out = []
    _str_pathfilename_txt = os.path.splitext(_str_pathfilename_pdf)[0] + '.txt'
    with open(_str_pathfilename_txt, 'r', encoding='utf-8') as _file_in_txt:
        _lst_line = _file_in_txt.readlines()
        _is_content_begin = False
        _is_content_end = False
        for _int_pos, _str_line in enumerate(_lst_line):
            _str_line.strip()
            _str_line = ' '.join(_str_line.split())  # remove multiple spaces
            if _str_line != '':
                for _str_flag in _odct_flags['Flags']['End']:
                    if _is_content_begin is True and _str_line.lower().find(_str_flag.lower()) != -1:
                        _is_content_end = True
                        break
                if _is_content_end is True:
                    break
                if _is_content_begin is True:
                    _lst_line_out.append(_str_line)
                for _str_flag in _odct_flags['Flags']['Start']:
                    if _is_content_begin is False and _str_line.lower().find(_str_flag.lower()) != -1:
                        _is_content_begin = True
                        break
    os.remove(_str_pathfilename_txt)
    
    print('\n'.join(_lst_line_out))
    _lst_line_out = merge_lines_non_digit(_lst_line_out)
    print('\n'.join(_lst_line_out))
    _lst_line_out = indent_content(_lst_line_out)
    
    # store .lst
    _str_pathfilename_lst = '_'.join(os.path.splitext(_str_pathfilename_pdf)[0].split('_')[:-1]) + '.lst'
    with open(_str_pathfilename_lst, 'w', encoding='utf-8') as _file_out_lst:
        _file_out_lst.write('\n'.join(_lst_line_out))

def main():
    _str_pathfilename_pdf = '/home/patrick/Dev/Wiki-citoyen/Data_in/2018-03-26T20-00_gCmnWbrBvcAdm_seance_consCmn/2018-03-26T20-00_gCmnWbrBvcAdm_seance_consCmn_odj_src.pdf'
    extract(_str_pathfilename_pdf)


if __name__ == '__main__':
    _dt_now_begin = datetime.datetime.now()
    str_prg_name = os.path.split(sys.argv[0])[1]
    print('Begin %s ' % (str_prg_name + ' ' + _dt_now_begin.strftime('%Y-%m-%d %H:%M:%S')))
    main()
    _dt_now_end = datetime.datetime.now()
    print('End %s ' % (str_prg_name + ' ' + _dt_now_end.strftime('%Y-%m-%d %H:%M:%S')))
    _dt_delta = _dt_now_end - _dt_now_begin
    print('Delta %s' % str(_dt_delta))