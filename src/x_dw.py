'''
Created on 16 janv. 2018

@author: patrick

Dokuwiki helpers

http://python-dokuwiki.readthedocs.io/en/latest/

Developper notes:
  the namespace used by the library are absolute without the leading : (this is not the unix convention)
  there are no relative path (no current working directory)
  the parameter of the functions follow unix convention to use os.path library
  the page class remove the file extension (which is allways .txt)

'''

import os
import re
import requests
# import base64

# https://pypi.python.org/pypi/dokuwiki/0.2
import dokuwiki


class X_dw(object):

    def update_dw_section_list(self, _x_ftp, _str_fullpathfilename_ftp, str_comment_flag, str_line):
        # insert a line inside a block surrounded by comment
        _str_page = _x_ftp.download_ftp_str_page(_str_fullpathfilename_ftp)
        _lst_lines = _str_page.split('\n')  # remove eol

        _has_link = False
        for _str_line in _lst_lines:
            if _str_line.find(str_line) != -1:
                _has_link = True
                break
        if _has_link is False:
            _lst_str_head = []
            _is_head = True
            _lst_str_backlink = []
            _lst_str_tail = []
            _is_tail = False
            if _has_link is False:
                for _str_line in _lst_lines:
                    if _is_head is True:
                        _lst_str_head.append(_str_line)
                        if _str_line.find('begin %s' % str_comment_flag) != -1:
                            _is_head = False
                        continue
                    if _str_line.find('end %s' % str_comment_flag) != -1:
                        _is_tail = True
                        _lst_str_tail.append(_str_line)
                        continue
                    if _is_tail is True:
                        _lst_str_tail.append(_str_line)
                        continue
                    _lst_str_backlink.append(_str_line)
            _lst_str_backlink.append(str_line)
            # remove empty lines
            _lst_str_backlink = list(filter(bool, _lst_str_backlink))
            _lst_str_backlink.sort()
            _str_page_new = '\n'.join(_lst_str_head) + '\n' \
                            + '\n'.join(_lst_str_backlink) + '\n' \
                            + ' \n' + '\n'.join(_lst_str_tail)
            _x_ftp.upload_str(_str_page_new, _str_fullpathfilename_ftp)

    def name_to_dw_name(self, str_name):
        # print('name_to_dw_name', str_name)
        str_name = str_name.lower()
        str_name = re.sub('[^-a-z._0-9]', '_', str_name)
        while str_name[0] in ['.', '-', '_']:
            str_name = str_name[1:]
        while str_name[-1] in ['.', '-', '_']:
            str_name = str_name[:-1]
        while str_name.find('__') != -1:
            str_name = str_name.replace('__', '_')
        return str_name

    def file_name_to_file_dw_name(self, str_file_name):
        _str_base = os.path.splitext(str_file_name)[0]
        _str_base = self.name_to_dw_name(_str_base)
        _str_file_dw_name = _str_base + os.path.splitext(str_file_name)[1]
        return _str_file_dw_name

    def path_name_to_path_dw_name(self, str_pathname):
        # This do NOT split extention (not require but more coherent)
        # _str_path_dw_name = '/'.join([X_dw().name_to_dwname(x) for x in str_pathname.split('/')])
        _lst_item_dw = []
        for _str_item in str_pathname.split('/'):
            _str_item_dw = self.file_name_to_file_dw_name(_str_item)
            _lst_item_dw.append(_str_item_dw)
        _str_path_dw_name = ''
        for _item in _lst_item_dw:
            _str_path_dw_name = os.path.join(_str_path_dw_name, _item)
        return _str_path_dw_name

    def __init__(self):
        pass


class X_dw_media(object):

    def ux_2_dw_path(self, str_path):
        if str_path[0] == '/':
            str_path = str_path[1:]
        return str_path.lower().replace('/', ':')

    def str_pathfilename_4_requests_media(self, str_pathfilename, str_basedokuwiki=None, str_domain=None,
                                          is_colon=True, is_rewrite=False, is_https=False):
        # an url could be presented differently depending of configuration
        if is_colon:
            str_pathfilename = str_pathfilename.replace('/', ':')
        else:
            str_pathfilename = str_pathfilename.replace(':', '/')
        if is_rewrite:
            str_pathfilename.replace('doku.php?id=', '')
        else:
            # str_pathfilename = 'doku.php?id=' + str_pathfilename
            str_pathfilename = 'lib/exe/fetch.php?media=' + str_pathfilename
        # the pages mame do not have .txt extension
        if str_pathfilename.endswith('.txt'):
            str_pathfilename = str_pathfilename[:-len('.txt')]
        if str_pathfilename is not None:
            str_pathfilename = os.path.join(str_basedokuwiki, str_pathfilename)
        if str_domain is not None:
            if is_https:
                str_pathfilename = 'https://' + str_domain + '/' + str_pathfilename
            else:
                str_pathfilename = 'http://' + str_domain + '/' + str_pathfilename
        return str_pathfilename

    def lst_dir_old(self, str_pathname):
        _lst_entry_pages = None
        try:
            _lst_entry_pages = self.x_dw.media.list('/' + str_pathname)  # , depth=1)
        except (dokuwiki.DokuWikiError, Exception) as err:
            print('DokuWikiError: %s' % err)
        return _lst_entry_pages

    def lst_dir(self, str_ux_pathname, str_pattern):
        # must have : instead of /
        # the returned list id do NOT contain the absolute heading ';'
        _lst_entry_pages = None
        try:
            if len(str_ux_pathname) == 0:
                _str_dw_pathname = '/'
            else:
                _str_dw_pathname = self.ux_2_dw_path(str_ux_pathname)
            _int_level = _str_dw_pathname.count(':')
            _str_zozo = 'projets:transparencia;institutions'
            _lst_entry_pages_00 = self.x_dw.dw_api.medias.list(_str_zozo, depth=0)
            _lst_entry_pages_01 = self.x_dw.dw_api.medias.list(_str_zozo, depth=1)
            '''
            _str_zozo = 'diaro:2016'
            _lst_entry_pages_02 = self.x_dw.dw_api.pages.list(_str_zozo, depth=_int_level + 1)
            '''
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname)  # , skipacl=True, pattern=str_pattern)
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname, depth=0)  # , skipacl=True, pattern=str_pattern)
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname, depth=1)  # , skipacl=True, pattern=str_pattern)
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname, depth=2)  # , skipacl=True, pattern=str_pattern)
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname, depth=3)  # , skipacl=True, pattern=str_pattern)
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname, depth=4)  # , skipacl=True, pattern=str_pattern)
            _lst_entry_pages = self.x_dw.dw_api.medias.list(_str_dw_pathname, depth=8)  # , skipacl=True, pattern=str_pattern)
        except (dokuwiki.DokuWikiError, Exception) as err:
            print('X_pages lst_dir DokuWikiError: %s' % err)
            print('str_ux_pathname : %s' % str_ux_pathname)
        return _lst_entry_pages

    def exists(self, str_pathfilename):
        _str_filename = os.path.basename(str_pathfilename)
        _str_pathname = os.path.dirname(str_pathfilename)
        _lst_entry_pages = self.lst_dir(_str_pathname)
        _exist = False
        if _lst_entry_pages is not None:
            for _entry in _lst_entry_pages:
                if _entry['id'] == str_pathfilename.replace('/', ':'):
                    _exist = True
                    break
        return _exist

    def download(self, str_pathfilename):
        _str_page = None
        try:
            _str_pathfilename_dw = X_dw.str_pathfilename_4_dw_page(str_pathfilename)
            _str_page = self.x_dw.media.get(_str_pathfilename_dw)
        except (dokuwiki.DokuWikiError, Exception) as err:
            print('DokuWikiError: %s' % err)
        return _str_page

    def upload_string_2_file(self, str_page='', str_ux_pathfilename='start'):
        if str_page is None or str_page == '':
            print('Empty page : %s' % str_ux_pathfilename)
        else:
            try:
                _str_dw_pathfilename = self.ux_2_dw_path(str_ux_pathfilename)
                # self.x_dw.dw_api.medias.set(_str_dw_pathfilename, base64.b64encode(str_page.encode('utf-8')))
                # self.x_dw.dw_api.medias.set(_str_dw_pathfilename, str_page.encode('utf-8'), b64encode=True)
                self.x_dw.dw_api.medias.set(_str_dw_pathfilename, str_page.encode('utf-8'))
            except (dokuwiki.DokuWikiError, Exception) as err:
                print('X_media upload unable to set: %s' % err)
            _str_url = self.str_pathfilename_4_requests_media(str_ux_pathfilename.lower(),
                                                              str_basedokuwiki=self.x_dw.str_web_rootname,
                                                              str_domain=self.x_dw.str_domain,
                                                              is_colon=True,
                                                              is_rewrite=False)
            # print(_str_url)
            _requests = requests.get(_str_url)
            if _requests.status_code != 200:
                print('store_card_in_dw - Requests get error  ' + str(_requests.status_code))

    def __init__(self, x_dw):
        self.x_dw = x_dw
        # aa = self.x_dw.dw_api.version
        # bb = dir(self.x_dw.dw_api.medias)


class X_dw_pages(object):

    def ux_2_dw_path(self, str_path):
        if str_path[0] == '/':
            str_path = str_path[1:]
        return os.path.splitext(str_path)[0].lower().replace('/', ':')

    def str_pathfilename_4_dw_page(self, str_pathfilename_in):
        ''' transform an local path to a xml-prc path '''

        str_pathfilename_dw = ''
        # not .txt extension for pages
        str_pathfilename_in = os.path.splitext(str_pathfilename_in)[0]
        # in dw the path is always lower case
        str_pathfilename_in.lower()
        # absolute path
        if not str_pathfilename_in.startswith(':') and not str_pathfilename_in.startswith('.'):
            str_pathfilename_dw = ':'
        str_pathfilename_dw += str_pathfilename_in.replace('/', ':')
        return str_pathfilename_dw

    def str_pathfilename_4_requests_page(self, str_pathfilename, str_basedokuwiki=None, str_domain=None,
                                         is_colon=True, is_rewrite=False, is_https=False):
        # an url could be presented differently depending of configuration
        str_pathfilename = os.path.splitext(str_pathfilename)[0]
        if is_colon:
            str_pathfilename = str_pathfilename.replace('/', ':')
        else:
            str_pathfilename = str_pathfilename.replace(':', '/')
        if is_rewrite:
            str_pathfilename.replace('doku.php?id=', '')
        else:
            str_pathfilename = 'doku.php?id=' + str_pathfilename
        # the pages mame do not have .txt extension
        if str_pathfilename.endswith('.txt'):
            str_pathfilename = str_pathfilename[:-len('.txt')]
        if str_pathfilename is not None:
            str_pathfilename = os.path.join(str_basedokuwiki, str_pathfilename)
        if str_domain is not None:
            if is_https:
                str_pathfilename = 'https://' + str_domain + '/' + str_pathfilename
            else:
                str_pathfilename = 'http://' + str_domain + '/' + str_pathfilename
        return str_pathfilename

    def lst_dir(self, str_ux_pathname, int_depth=0):
        # the returned list id do NOT contain the absolute heading ';'
        _lst_entry_pages = None
        try:
            if len(str_ux_pathname) == 0:
                _str_dw_pathname = '/'
            else:
                _str_dw_pathname = self.ux_2_dw_path(str_ux_pathname)
            _int_level = _str_dw_pathname.count(':')
            '''
            _str_zozo = ':diaro:2016'
            _lst_entry_pages_01 = self.x_dw.dw_api.pages.list(_str_zozo, depth=_int_level + 1)
            _str_zozo = 'diaro:2016'
            _lst_entry_pages_02 = self.x_dw.dw_api.pages.list(_str_zozo, depth=_int_level + 1)
            '''
            _lst_entry_pages = self.x_dw.dw_api.pages.list(_str_dw_pathname, depth=_int_level + 2 + int_depth)
        except (dokuwiki.DokuWikiError, Exception) as err:
            print('X_pages lst_dir DokuWikiError: %s' % err)
            print('str_ux_pathname : %s' % str_ux_pathname)
        return _lst_entry_pages

    def scan_dir(self):
        pass

    def exists_pathfilename(self, str_ux_pathfilename):
        _lst_entry_pages = self.lst_dir(os.path.dirname(str_ux_pathfilename))
        _str_dw_pathfilename = self.ux_2_dw_path(str_ux_pathfilename)
        _exist = False
        if _lst_entry_pages is not None:
            for _entry in _lst_entry_pages:
                if _entry['id'] == _str_dw_pathfilename:
                    _exist = True
                    break
        return _exist

    def download(self, str_ux_pathfilename):
        _str_page = None
        try:
            # _str_pathfilename_dw = self.x_dw.str_pathfilename_4_dw_page(str_ux_pathfilename)
            _str_dw_pathfilename = self.ux_2_dw_path(str_ux_pathfilename)
            _str_page = self.x_dw.dw_api.pages.get(_str_dw_pathfilename)
        except (dokuwiki.DokuWikiError, Exception) as err:
            print('X_pages download DokuWikiError: %s' % err)
        return _str_page

    def upload(self, str_page='', str_ux_pathfilename='start.txt'):
        # the str_pathfilename is unix style
        if str_page is None or str_page == '':
            print('Empty page : %s' % str_ux_pathfilename)
        else:
            try:
                str_dw_pathfilename = self.ux_2_dw_path(str_ux_pathfilename)
                # self.x_dw.dw_api.pages.set(self.x_dw.str_pathfilename_4_dw_page(str_pathfilename), str_page)
                self.x_dw.dw_api.pages.set(str_dw_pathfilename, str_page)
            except (dokuwiki.DokuWikiError, Exception) as err:
                print('X_pages upload unable to set: %s' % err)
            _str_url = self.str_pathfilename_4_requests_page(str_ux_pathfilename,
                                                             str_basedokuwiki=self.x_dw.str_web_rootname,
                                                             str_domain=self.x_dw.str_domain,
                                                             is_colon=True,
                                                             is_rewrite=False)
            # print(_str_url)
            _requests = requests.get(_str_url)
            if _requests.status_code != 200:
                print('store_card_in_dw - Requests get error  ' + str(_requests.status_code))

    def __init__(self, x_dw):
        self.x_dw = x_dw


class X_dw_rpc(object):

    @property
    def x_pages(self):
        return X_dw_pages(self)

    @property
    def x_media(self):
        return X_dw_media(self)

    def __init__(self, str_domain, str_web_rootname, str_usr, str_pwd, is_https=True):
        self.dw_api = None
        self.str_web_rootname = str_web_rootname
        self.str_domain = str_domain
        _str_http = 'https:'
        if is_https is False:
            _str_http
        _str_url = _str_http + '//' + str_domain + '/' + str_web_rootname
        try:
            self.dw_api = dokuwiki.DokuWiki(_str_url, str_usr, str_pwd)
        except (dokuwiki.DokuWikiError, Exception) as err:
            print('unable to connect: %s' % err)
