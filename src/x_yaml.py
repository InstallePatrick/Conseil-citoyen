'''
Created on 16 janv. 2018

@author: patrick
'''

import yaml
import yaml_orderedDict

class X_yaml(object):

    def dump_odct_to_str(self, odct_out):
        _str_out = yaml.dump(odct_out, 
                              default_flow_style=False, 
                              width=1000, 
                              encoding=('utf-8'),
                              allow_unicode=True,
                              Dumper=yaml_orderedDict.OrderedDumper)
        return _str_out

    def load_filename_2_odct(self, str_pathfilename):
        _odct = None
        with open(str_pathfilename, 'r') as _file_in:
            _odct = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
        return _odct