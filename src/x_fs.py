'''
Created on 3 févr. 2018

@author: patrick

This program is organized as standalone (executed directly as python script)

'''
import os
import sys
import datetime
import codecs

import lxml
import lxml.html
from lxml import etree as ET
from lxml.html.diff import htmldiff, html_annotate

import difflib
# https://github.com/seperman/deepdiff
# sudo pip install deepdiff
from deepdiff import DeepDiff  # For Deep Difference of 2 objects

# from unittest.test.testmock.support import is_instance

dct_src = {'str_path_base' : '',
           'str_pathname_filter' : '',
           'str_filename_filter' : '',
           }

dct_dst = {
    'str_path_base' : '',
    'Ftp': {
        'user': '',
        'pwd': '',
        }
    }


class X_fs(object):

    def move_local_to_filter_local(self, dct_src, dct_dst0):
        '''
        '''
        pass

    def ftp_local_to_remote(self, dct_src, dct_dst0):
        '''
        '''
        pass

    def last_archive_file_old(self, str_pathfilename, str_subdir='Archive'):
        '''
        Old
        '''
        _str_last = None
        _str_basename = os.path.basename(str_pathfilename)
        _lst_dir = os.listdir(os.path.join(str_pathfilename, str_sub_dir))
        for _str_dir in _lst_dir:
            if _str_dir.find(_str_basename) == 10:
                if _str_last < _str_dir:
                    _str_last = _str_dir
        return _str_last

    def save_data_as_local_file_old(self, _data, str_path_base, str_path_subdir, str_filename):
        '''

        '''
        _str_pathfilename = os.path.join(self.cfg['Bos']['Local']['path_store'],
                                         str_path_subdir,
                                         str_filename)
        if isinstance(_data, str):
            with codecs.open(_str_pathfilename, 'w', 'UTF-8') as _file_out:
                _file_out.write(_data)
        else:
            with open(_str_pathfilename, 'wb') as _file_out:
                _file_out.write(_data)

    def dct_fullnames(self, dct_names):
        dct_join = {}
        dct_join['path_filename'] = os.path.join(dct_names['path_base'],
                                                 dct_names['path_subdir'],
                                                 dct_names['filename'],
                                                 )
        _str_dir = os.path.split(dct_join['path_filename'])[0]
        if not os.path.exists(_str_dir):
            os.makedirs(_str_dir)
        if dct_names['path_archive_base'] is not None:
            dct_join['path_archive_filename'] = os.path.join(dct_names['path_base'],
                                                             dct_names['path_archive_base'],
                                                             dct_names['path_subdir'],
                                                             dct_names['iso_dt_now'].replace(':', '-')
                                                             + '_'
                                                             + dct_names['filename'],
                                                             )
            _str_dir = os.path.split(dct_join['path_archive_filename'])[0]
            if not os.path.exists(_str_dir):
                os.makedirs(_str_dir)

        return dct_join

    def save_local_diff(self,
                        data_new='',
                        data_old='',
                        dct_fullnames={},
                        ):
        # dff_html
        _str_diff = htmldiff(data_new, data_new)
        _str_html = '<html><head></head><body>'
        _str_html += _str_diff
        _str_html += '\n<hr>\n</body>'
        # print(_str_html)
        _str_fullpathfilename_diff_html = os.path.splitext(dct_fullnames['path_archive_filename'])[0] + '_lxml-html-diff.html'
        _str_fullpathfilename_diff_html = os.path.splitext(dct_fullnames['path_archive_filename'])[0] + '_lxml-html-diff.html'
        with codecs.open(_str_fullpathfilename_diff_html, 'w', 'utf-8') as _file:
            _file.write(_str_html)

        # unified_diff
        _lst_lines_last = data_new.splitlines()
        _lst_lines_new = data_new.splitlines()
        _lst_diff_lines = list(difflib.unified_diff(_lst_lines_last, _lst_lines_new))
        _str_diff = '\n'.join(_lst_diff_lines)
        # print(_str_diff)
        _str_fullpathfilename_diff_unified = os.path.splitext(dct_fullnames['path_archive_filename'])[0] + '_unified-diff.txt'
        with codecs.open(_str_fullpathfilename_diff_unified, 'w', 'utf-8') as _file:
            _file.write(_str_diff)

        _str_diff = difflib.HtmlDiff(wrapcolumn=72).make_table([line for line in data_new.split('\n') if len(line.strip()) > 0],
                                                               [line for line in data_old.split('\n') if len(line.strip()) > 0])
        # print(_str_diff)
        _str_fullpathfilename_html_diff = os.path.splitext(dct_fullnames['path_archive_filename'])[0] + '_difflilb-hmtl-diff.html'
        with codecs.open(_str_fullpathfilename_html_diff, 'w', 'utf-8') as _file:
            _file.write(_str_diff)

    def save_local_archive(self,
                           data_new='',
                           data_old='',
                           dct_fullnames={},
                           do_diff=True,
                           str_io_method_tail=''
                           ):

            with open(dct_fullnames['path_archive_filename'], 'w' + str_io_method_tail) as _file_out:
                _file_out.write(data_new)
            if data_old is not None and do_diff and (str_io_method_tail == ''):
                self.save_local_diff(data_new, data_old, dct_fullnames)

    def save_local_archive_diff(self,
                                data='',
                                dct_names=None,
                                do_diff=True):
        '''
        Not finalized
        '''
        # check validity
        if dct_names is None:
            dct_names = {'path_base': '.',
                         'path_archive_base': None,
                         'path_subdir': '',
                         'filename': 'dummy.txt',
                         }

        # Is string or bin ?
        _str_io_method_tail = ''
        if not isinstance(data, str):
            _str_io_method_tail = 'b'

        _dct_fullnames = self.dct_fullnames(dct_names)

        if os.path.exists(_dct_fullnames['path_filename']):
            _data_old = None
            with open(_dct_fullnames['path_filename'], 'r' + _str_io_method_tail) as _file_old:
                _data_old = _file_old.read()

            if data != _data_old:
                with open(_dct_fullnames['path_filename'], 'w' + _str_io_method_tail) as _file_out:
                    _file_out.write(data)

                if dct_names['path_archive_base'] is not None:
                    self.save_local_archive(data, _data_old, _dct_fullnames, do_diff, _str_io_method_tail)
        else:  # new
            with open(_dct_fullnames['path_filename'], 'w' + _str_io_method_tail) as _file_out:
                _file_out.write(data)
            if dct_names['path_archive_base'] is not None:
                _do_diff = False
                self.save_local_archive(data, None, _dct_fullnames, _do_diff, _str_io_method_tail)

    def __init__(self):
        pass


if __name__ == '__main__':

    X_fs().move_local_to_filter_local(dct_src, dct_dst)
