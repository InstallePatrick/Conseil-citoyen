'''
Created on 15 janv. 2018

@author: patrick
'''
import os
import time
import datetime
import codecs
import io
import re
import shutil
import collections

import lxml
import lxml.html
from lxml import etree as ET
from lxml.html.diff import htmldiff, html_annotate

import difflib
# https://github.com/seperman/deepdiff
# sudo pip install deepdiff
from deepdiff import DeepDiff  # For Deep Difference of 2 objects

import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException

# to interact with browser window (download .jpg, ...)
# https://pyautogui.readthedocs.io/en/latest/
# https://automatetheboringstuff.com/chapter18/
import pyautogui

from x_fs import X_fs


class X_selenium(object):

    str_main_window_name = 'BOS - council online - Mozilla Firefox'
    str_pdf = ''
    str_pdf_download = ''

    '''
    # http://www.obeythetestinggoat.com/how-to-get-selenium-to-wait-for-page-load-after-a-click.html
    @contextmanager
    def wait_for_page_load(self, timeout=30):
        old_page = self.browser.find_element_by_tag_name('html')
        yield
        WebDriverWait(self.cfg[self.cfg_root]['Selenium']['driver'], timeout).until(
            staleness_of(old_page)
        )
    '''

    def wait_for_element_xpath(self, str_xpath='//body', int_delay=16):
        try:
            # wait = WebDriverWait(self.cfg[self.cfg_root]['Selenium']['driver'], int_delay)
            wait = WebDriverWait(self.driver, int_delay)
            wait.until(EC.presence_of_element_located((By.XPATH, str_xpath)))
            # print ("Page is ready!")
        except TimeoutException:
            print ("Loading took too much time! %s" % str_xpath)
        time.sleep(2)  # anyway

    def load_xpath(self, str_xpath):
        # move to Selenium_utils(self.cfg).load_xpath(str_xpath) ?
        _elm_block_data = None
        _is_waiting_page = True
        _int_retry = 3
        while _is_waiting_page and (_int_retry > 0):
            _int_retry -= 1
            try:
                _elm_block_data = self.cfg_root['Selenium']['driver'].find_element_by_xpath(str_xpath)
                _is_waiting_page = False
            # except selenium.common.exceptions.NoSuchElementException as e:
            except NoSuchElementException as e:
                print(e)
                time.sleep(8)
        return _elm_block_data

    def download_pdf_old(self):
        # titre de la fenêtre le texte du line (sans 'fn')
        # https://stackoverflow.com/questions/10629815/handle-multiple-window-in-python/29982298#29982298
        _str_title = self.cfg_root['Selenium']['driver'].title
        _str_window_title_main = 'BOS - council online - Mozilla Firefox'
        _str_window_title_pdf = ''
        _str_window_title_pdf_download = ''
        _selm_link = self.cfg_root['Selenium']['driver'].find_element_by_xpath("//a[@id='fr']")
        _selm_link.send_keys(Keys.CONTROL+'p')  # print

    def selm_as_clean_html(self, selm_block_data):
        # old known as selm_as_clean_et

        # used for files
        _str_html_block_new = selm_block_data.get_attribute('outerHTML')

        _parser = ET.HTMLParser()
        _html_tree = ET.parse(io.StringIO(_str_html_block_new), _parser)

        # Tooltip contain publisched delay
        _lst_tooltip = _html_tree.xpath('//div[@class="bosecr-tooltip"]')
        for _el_tooltip in _lst_tooltip:
            _el_tooltip.getparent().remove(_el_tooltip)

        _byt_html_tree = ET.tostring(_html_tree.getroot(), pretty_print=True)
        # print('************************************************************************')
        # print(str(_byt_tree, 'UTF-8'))

        _str_html_tree = str(_byt_html_tree, 'UTF-8')
        # clean left white space
        _lst_lgns = _str_html_tree.split('\n')
        _str_html_tree_clean = ''
        for _str_lgn in _lst_lgns:
            _str_lgn = _str_lgn.rstrip()
            _str_lgn = _str_lgn.replace('\t', '  ')
            if len(_str_lgn) != 0:
                _str_html_tree_clean += _str_lgn + '\n'
        # print('************************************************************************')
        # print(_str_tree_clean)

        # displayNotes=true&amp;_=1514505027897"  -> displayNotes=true"
        _str_html_tree_clean = re.sub(r'(displayNotes=true)(&amp;_=\d+)', '\g<1>', _str_html_tree_clean)

        return _str_html_tree_clean

    def element_selenium_2_lxml(self, selm_block_data):
        '''
        convert and clean
        used for xpath
        '''
        _str_tree_clean = self.selm_as_clean_html(selm_block_data)
        _parser = ET.HTMLParser()
        _et_page = ET.parse(io.StringIO(_str_tree_clean), _parser).getroot()
        # print(str(ET.tostring(_et_page, pretty_print=True), 'UTF-8'))
        _et_out = _et_page.xpath('/html/body/*')[0]
        # print(str(ET.tostring(_et_out, pretty_print=True), 'UTF-8'))
        return (_et_out)

    def store_meetingDetails_cons_diff(self, str_last, str_new):
        '''
        TODO pretty html ?
        '''
        # htmldiff
        _str_diff = htmldiff(str_last, str_new)
        # print(_str_diff)

        # unified_diff
        _lst_diff_lines = list(difflib.unified_diff(str_last.splitlines(), str_new.splitlines(), 0))
        _str_diff = '\n'.join(_lst_diff_lines)
        # print(_str_diff)

        _str_diff = difflib.HtmlDiff(wrapcolumn=72).make_table([line for line in str_last.split('\n') if len(line.strip()) > 0],
                                                               [line for line in str_new.split('\n') if len(line.strip()) > 0])
        # print(_str_diff)
        '''
        _str_full_path = os.path.join(_str_httpd_root, _str_folder, _str_filename)
        #with open(_str_full_path, 'w', encoding='utf-8') as _file:
        with codecs.open(_str_full_path, 'w', 'utf-8') as _file:
            _file.write('\n'.join(_lst_diff_lines))
        '''

    def save_meetingDetails_cons_block(self, str_block_path_name, _elm_block_data):
        # store the lastDiffusedDocs_portlet
        # TODO Diff

        _str_html_block_old = None
        _str_block_file_name = 'meetingDetails_%(lang)s' % {'lang': self.str_lang} + '.html'
        _str_block_path_file_name = os.path.join(str_block_path_name, _str_block_file_name)
        if os.path.exists(_str_block_path_file_name):
            with open(_str_block_path_file_name, 'r') as _file_block_old:
                _str_html_block_old = _file_block_old.read()

        _str_html_block_new = self.selm_as_clean_html(_elm_block_data)

        if _str_html_block_old is None or (_str_html_block_old != _str_html_block_new):
            # store new block
            # _str_block_file_name = _str_block_file_name
            with open(os.path.join(str_block_path_name, _str_block_file_name), 'w') as _file_block_new:
                _file_block_new.write(_str_html_block_new)
            # store for archive
            _str_dt_now = self.cfg_root['dt_now'].strftime('%Y-%m-%dT%H-%M-%S')
            _str_block_file_name = _str_dt_now + '_' + _str_block_file_name
            with open(os.path.join(str_block_path_name, _str_block_file_name), 'w') as _file_block_new:
                _file_block_new.write(_str_html_block_new)
            # store diff
            if _str_html_block_old is not None:
                self.store_meetingDetails_cons_diff(_str_html_block_old, _str_html_block_new)

    def store_diff(self, str_pathfilename, str_last, str_new):
        '''
        TODO pretty html ?
        '''
        _str_diff = htmldiff(str_last, str_new)
        _str_html = '<html><head></head><body>'
        _str_html += _str_diff
        _str_html += '\n<hr>\n</body>'
        # print(_str_html)
        with codecs.open(str_pathfilename + '_lxml-html-diff.html', 'w', 'utf-8') as _file:
            _file.write(_str_html)

        # unified_diff
        _lst_lines_last = str_last.splitlines()
        _lst_lines_new = str_new.splitlines()
        _lst_diff_lines = list(difflib.unified_diff(_lst_lines_last, _lst_lines_new))
        _str_diff = '\n'.join(_lst_diff_lines)
        # print(_str_diff)
        with codecs.open(str_pathfilename + '_unified-diff.txt', 'w', 'utf-8') as _file:
            _file.write(_str_diff)

        _str_diff = difflib.HtmlDiff(wrapcolumn=72).make_table([line for line in str_last.split('\n') if len(line.strip()) > 0],
                                                               [line for line in str_new.split('\n') if len(line.strip()) > 0])
        # print(_str_diff)
        with codecs.open(str_pathfilename + '_difflilb-hmtl-diff.html', 'w', 'utf-8') as _file:
            _file.write(_str_diff)

    def save_page_block(self, elm_block_data, dct_names):
        # store the lastDiffusedDocs_portlet

        _str_html_block_new = self.selm_as_clean_html(elm_block_data)
        X_fs().save_local_archive_diff(_str_html_block_new,
                                       dct_names,
                                       True)
        '''

        if os.path.exists(_str_pathfilename_block):
            with open(_str_pathfilename_block, 'r') as _file_block_old:
                _str_html_block_old = _file_block_old.read()
        '''
        '''
        if _str_html_block_old is None or (_str_html_block_old != _str_html_block_new):
            # store new block
            # _str_block_file_name = _str_block_file_name
            with open(os.path.join(str_block_path_name, _str_filename_block), 'w') as _file_block_new:
                _file_block_new.write(_str_html_block_new)
            # store for archive
            _str_dt_now = self.cfg[self.cfg_root]['dt_now'].strftime('%Y-%m-%dT%H-%M-%S')
            _str_filename_dt_block = _str_dt_now + '_' + _str_filename_block
            with open(os.path.join(str_block_path_name, _str_filename_dt_block), 'w') as _file_block_new:
                _file_block_new.write(_str_html_block_new)
            # store diff
            if _str_html_block_old is not None:
                self.store_diff(os.path.join(str_block_path_name, os.path.splitext(_str_filename_dt_block)[0]),
                                _str_html_block_old,
                                _str_html_block_new)
        '''

    def save_lastDiffusedDocs_portlet_old(self, str_local_subpath, str_xpath):
        # store and ?diff?

        # print(self.cfg[self.cfg_root]['Selenium']['driver'].page_source)
        _str_xpath = "//div[@id='dashboardPortletDiffusedDocWrapper']"
        _str_block_path_name = os.path.join('.', 'Data', self.cfg_root['config_name'])
        if not os.path.exists(_str_block_path_name):
            os.mkdir(_str_block_path_name)

        # self.store_block(_str_block_path_name, _elm_block_data, 'innerHTML')
        _elm_block_data = self.cfg_root['Selenium']['driver'].find_element_by_xpath(_str_xpath)
        self.save_lastDiffusedDocs_block(_str_block_path_name, _elm_block_data)  # , 'outerHTML')

    def get_popup_window_handle(self):
        popup_window_handle = None
        while not popup_window_handle:
            for handle in self.cfg_root['Selenium']['driver'].window_handles:
                if handle != self.cfg_root['Selenium']['main_window_handle']:
                    popup_window_handle = handle
                    break
        self.cfg_root['Selenium']['driver'].switch_to.window(popup_window_handle)

    def get_main_window_handle(self):
        main_window_handle = None
        while not main_window_handle:
            main_window_handle = self.cfg_root['Selenium']['driver'].current_window_handle
        self.cfg_root['Selenium']['main_window_handle'] = main_window_handle
        return main_window_handle

    def upload_content(self):
        pass

    def get_driver_custom(self):
        '''
        force download .pdf
        N.B. Fail 2017-12-19 :-(
        '''
        profile = webdriver.FirefoxProfile()

        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.helperApps.alwaysAsk.force", False)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.download.dir", os.getcwd())

        # below line was missing in yours

        profile.set_preference("plugin.disable_full_page_plugin_for_types", "application/pdf")
        profile.set_preference("pdfjs.disabled", True)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")

        driver = webdriver.Firefox(firefox_profile=profile)
        self.driver = driver
        return driver

    def get_driver(self):
        self.driver = webdriver.Firefox()
        if 'Selenium' not in self.cfg_root:
            self.cfg_root['Selenium'] = collections.OrderedDict()
        self.cfg_root['Selenium']['driver'] = self.driver

    def __init__(self, cfg, str_root=None):
        self.cfg_root = cfg
        if str_root != None:
            self.cfg_root = cfg[str_root]
        self.get_driver()

