'''
Created on 12 mars 2018

@author: patrick
'''
import datetime
import os
import yaml
import yaml_orderedDict

from x_yaml import X_yaml


def read_file(lst_pathfilename):
    _str_in = None
    with open(os.path.join(*lst_pathfilename), 'r') as _file_in:
        _str_in = _file_in.read()
    return _str_in


def read_file_string(lst_pathfilename):
    _str_in = None
    with open(os.path.join(*lst_pathfilename), 'r', encoding='utf-8') as _file_in:
        _str_in = _file_in.read()
    return _str_in


def read_file_bytes(lst_pathfilename):
    _str_in = None
    with open(os.path.join(*lst_pathfilename), 'rb') as _file_in:
        _str_in = _file_in.read()
    return _str_in


def mixedCase_2_list(str_mixed):
    _lst_items = []
    _lst_char = []
    for _char in str_mixed:
        if _char == _char.upper():
            _lst_char.append('-')
        _lst_char.append(_char)
    _str_with_separator = ''.join(_lst_char)
    _lst_item = _str_with_separator.split('-')
    return _lst_item


def merge_2dicts(a, b, path=None, update=True):
    "http://stackoverflow.com/questions/7204805/python-dictionaries-of-dictionaries-merge"
    "merges b into a"
    if b is not None:
        if path is None:
            path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    merge_2dicts(a[key], b[key], path + [str(key)])
                elif a[key] == b[key]:
                    pass  # same leaf value
                elif isinstance(a[key], list) and isinstance(b[key], list):
                    for idx, val in enumerate(b[key]):
                        a[key][idx] = merge_2dicts(a[key][idx], b[key][idx], path + [str(key), str(idx)], update=update)
                elif update:
                    a[key] = b[key]
                else:
                    raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
            else:
                a[key] = b[key]
    return a


def trigramme_cmn_2_name(str_trg, do_fullname=True):
    # split and keep 3 first tokens ?

    _dct_trg_name = {
                    'gCmnBruAnd': "commune d'Anderlecht",
                    'gCmnBruAud': "commune d'Auderghem",
                    'gCmnBruBer': "commune de Berchem-Sainte-Agathe",
                    'gCmnBruEtt': "commune d'Etterbeek",
                    'gCmnBruEve': "commune d'Evere",
                    'gCmnBruFor': "commune de Forest",
                    'gCmnBruGan': "commune de Ganshoren",
                    'gCmnBruIxl': "commune d'Ixelles",
                    'gCmnBruJet': "commune de Jette",
                    'gCmnBruKoe': "commune de Koekelberg",
                    'gCmnBruMol': "commune de Molenbeek-Saint-Jean",
                    'gCmnBruStg': "commune de Saint-Gilles",
                    'gCmnBruStj': "commune de Saint-Josse-ten-Noode",
                    'gCmnBruSch': "commune de Schaerbeek",
                    'gCmnBruUcc': "commune d'Uccle",
                    'gCmnBruBru': "ville de Bruxelles",
                    'gCmnBruWat': "commune de Watermael-Boitsfort",
                    'gCmnBruWsl': "commune de Woluwe-Saint-Lambert",
                    'gCmnBruWsp': "commune de Woluwe-Saint-Pierre",

                    'gCmnWbrBvc': "commune de Beauvechain",
                    'gCmnWbrTub': "ville de Tubize",
                    'gCmnWbrOlln': "ville d'Ottignies-Louvain-la-Neuve",

                    'gPrvWlnWbr': 'province du Brabant wallon',
                    }
    _odct_ngram = None
    _str_filename_ngram = './Lib/ngram.yaml'
    if os.path.exists(_str_filename_ngram):
        _odct_ngram = X_yaml().load_filename_2_odct(_str_filename_ngram)

    _lst_trigram = mixedCase_2_list(str_trg)
    if do_fullname:
        # _str_fullname = _dct_trg_name[''.join(_lst_trigram[:4])]
        _str_fullname = _odct_ngram['Cfg']['N-gram'][''.join(_lst_trigram[:4])]['class_and_name']
    else:
        _str_fullname = _odct_ngram['Cfg']['N-gram'][''.join(_lst_trigram[:4])]['name']
    # return _dct_trg_name[str_trg[:10]]
    return _str_fullname


def dt_iso_2_date_hmn_fr(str_dt_iso, with_time=False):
    _dct_weekday_en_fr = {
        'Mon': 'lundi',
        'Tue': 'mardi',
        'Wed': 'mercredi',
        'Thu': 'jeudi',
        'Fri': 'vendredi',
        'Sat': 'samedi',
        'Sun': 'dimanche',
        }
    _dct_month_en_fr = {
                        'Jan': 'janvier',
                        'Feb': 'février',
                        'Mar': 'mars',
                        'Apr': 'avril',
                        'May': 'mai',
                        'Jun': 'juin',
                        'Jul': 'juillet',
                        'Aug': 'août',
                        'Sep': 'septembre',
                        'Oct': 'octobre',
                        'Nov': 'novembre',
                        'Dec': 'décembre',
        }
    # not clean but efficient
    if str_dt_iso.find(':') != -1:
        str_dt_iso = str_dt_iso.replace(':', '-')
    _dt = datetime.datetime.strptime(str_dt_iso, '%Y-%m-%dT%H-%M')
    _str_out = ''
    _str_out += _dct_weekday_en_fr[_dt.strftime('%a')]
    _str_out += ' %s ' % _dt.strftime('%d')
    _str_out += _dct_month_en_fr[_dt.strftime('%b')]
    _str_out += ' ' + _dt.strftime('%Y')
    if with_time:
        _str_out += _dt.strftime(' à %Hh%M')
    return _str_out


def dt_iso_2_datetime_hmn_fr(str_dt_iso):
    _str_datetime = dt_iso_2_date_hmn_fr(str_dt_iso)
    if str_dt_iso.find(':') != -1:
        str_dt_iso = str_dt_iso.replace(':', '-')
    _dt = datetime.datetime.strptime(str_dt_iso, '%Y-%m-%dT%H-%M')
    _str_datetime += _dt.strftime(' à %Hh%M')
    return

class X_config(object):
    '''
    classdocs
    '''

    def load_config_private(self):
        _cfg = None
        _str_path_file_name = os.path.join('../Config', os.path.splitext(self.str_name)[0] + '_private' + '.yaml')
        if os.path.exists(_str_path_file_name):
            _cfg = X_yaml().load_filename_2_odct(_str_path_file_name)
        return _cfg

    def load_config(self, str_name):
        self.str_name = str_name
        _cfg = None

        _str_path_file_name = os.path.join('../Config', os.path.splitext(str_name)[0] + '.yaml')
        if os.path.exists(_str_path_file_name):
            with open(_str_path_file_name, 'r') as _file_in:
                _cfg = yaml.load(_file_in, Loader=yaml_orderedDict.OrderedLoader)
        _cfg_org = self.load_config_private()
        _cfg = merge_2dicts(_cfg, _cfg_org)

        return _cfg

    def __init__(self):  # , str_prg_name, str_cfg_name=None):
        '''
        Constructor
        '''
        pass

